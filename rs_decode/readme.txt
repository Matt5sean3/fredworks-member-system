
This folder contains a subset of the fec-3.0.1 package source written by
Phil Karn, KA9Q.

The full source of that package may be obtained from
http://www.ka9q.net/code/fec/

That source is used here under the LGPLv2 license which that software is
distributed with.

The only additions to that source are the files seen in the "external"
subdirectory and this readme file.

