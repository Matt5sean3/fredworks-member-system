// This is a custom header file that can be used by external files
//
#ifndef RS_DECODE_H_
#define RS_DECODE_H_

// Needs to be used from C++, respect C linkage
#ifdef __cplusplus
extern "C" {
#endif

void
free_rs_char( void * p );

void * init_rs_char(
  int symsize,
  int gfpoly,
  int fcr,
  int prim,
  int nroots,
  int pad );

int decode_rs_char(
  void * p,
  unsigned char * data,
  int * eras_pos,
  int no_eras );

void
free_rs_int( void * p );

void * init_rs_int(
  int symsize,
  int gfpoly,
  int fcr,
  int prim,
  int nroots,
  int pad );

int decode_rs_int(
  void * p,
  unsigned int * data,
  int * eras_pos,
  int no_eras );

#ifdef __cplusplus
}
#endif

#endif

