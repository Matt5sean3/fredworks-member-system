
#ifndef FredWorksMemberCardWindow_h_
#define FredWorksMemberCardWindow_h_

#include <gtk/gtk.h>

#define FREDWORKS_MEMBER_CARD_WINDOW_TYPE ( fredworks_member_card_window_get_type( ) )
G_DECLARE_FINAL_TYPE( FredWorksMemberCardWindow, fredworks_member_card_window, FREDWORKS_MEMBER_CARD, WINDOW, GtkWindow )

FredWorksMemberCardWindow * fredworks_member_card_window_new(
  const gchar * memberName,
  gint expiration,
  const char * fingerprint );

void fredworks_member_card_window_update(
  FredWorksMemberCardWindow * win,
  const gchar * memberName,
  gint expiration,
  const char * fingerprint );

#endif

