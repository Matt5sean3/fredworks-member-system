
#ifndef FredWorksMembershipAppWindow_h_
#define FredWorksMembershipAppWindow_h_

#include <gtk/gtk.h>
#include "FredWorksMembershipApp.h"

#define FREDWORKS_MEMBERSHIP_APP_WINDOW_TYPE ( fredworks_membership_app_window_get_type( ) )
G_DECLARE_FINAL_TYPE( FredWorksMembershipAppWindow, fredworks_membership_app_window, FREDWORKS_MEMBERSHIP, APP_WINDOW, GtkApplicationWindow )

FredWorksMembershipAppWindow * fredworks_membership_app_window_new( FredWorksMembershipApp * app );
void fredworks_membership_app_window_open( FredWorksMembershipAppWindow * win, GFile * file );

enum {
  FREDWORKS_MEMBERSHIP_FIELD_STORE_KEY_COLUMN = 0,
  FREDWORKS_MEMBERSHIP_FIELD_STORE_VALUE_COLUMN = 1
};

#endif

