
#include "FredWorksMembershipApp.h"
#include "FredWorksMembershipAppWindow.h"

struct _FredWorksMembershipApp
{
  GtkApplication parent;
};

typedef struct _FredWorksMembershipAppPrivate FredWorksMembershipAppPrivate;

struct _FredWorksMembershipAppPrivate
{
  gchar * keyStoreFile;
};

G_DEFINE_TYPE_WITH_PRIVATE( FredWorksMembershipApp, fredworks_membership_app, GTK_TYPE_APPLICATION );

const char *
fredworks_membership_app_get_key_store_file( FredWorksMembershipApp * app )
{
  FredWorksMembershipAppPrivate * priv =
    fredworks_membership_app_get_instance_private( FREDWORKS_MEMBERSHIP_APP( app ) );
  return priv->keyStoreFile;
}

static void
fredworks_membership_app_init( FredWorksMembershipApp * app )
{
  FredWorksMembershipAppPrivate * priv =
    fredworks_membership_app_get_instance_private( FREDWORKS_MEMBERSHIP_APP( app ) );

}

static void
fredworks_membership_app_activate( GApplication * app )
{
  FredWorksMembershipAppPrivate * priv =
    fredworks_membership_app_get_instance_private( FREDWORKS_MEMBERSHIP_APP( app ) );

  /*
  if( priv->keyStoreFile )
  {
    printf( "Using key store file: %s\n", priv->keyStoreFile );
  }
  else
  {
    printf( "Using local key store\n" );
  }
  */

  // Use the sub-classed GtkApplicationWindow
  FredWorksMembershipAppWindow * win;
  win = fredworks_membership_app_window_new( FREDWORKS_MEMBERSHIP_APP( app ) );
  gtk_window_present( GTK_WINDOW( win ) );
}

static void
fredworks_membership_app_class_init( FredWorksMembershipAppClass * class )
{
  G_APPLICATION_CLASS( class )->activate = fredworks_membership_app_activate;
}

FredWorksMembershipApp *
fredworks_membership_app_new( void )
{
  FredWorksMembershipApp * app = g_object_new(
      FREDWORKS_MEMBERSHIP_APP_TYPE,
      "application-id", "org.fwks.membership",
      "flags", G_APPLICATION_FLAGS_NONE,
      NULL );

  FredWorksMembershipAppPrivate * priv =
    fredworks_membership_app_get_instance_private( FREDWORKS_MEMBERSHIP_APP( app ) );
  priv->keyStoreFile = NULL;

  GOptionEntry appOptions[2];

  appOptions[ 0 ].long_name = "key-store";
  appOptions[ 0 ].short_name = 'k';
  appOptions[ 0 ].flags = G_OPTION_FLAG_NONE;
  appOptions[ 0 ].arg = G_OPTION_ARG_FILENAME;
  appOptions[ 0 ].arg_data = &priv->keyStoreFile;
  appOptions[ 0 ].description = "The path to a key-file to utilize instead of the local key store";
  appOptions[ 0 ].arg_description = "key-file";

  appOptions[ 1 ].long_name = NULL;
  appOptions[ 1 ].short_name = 0;
  appOptions[ 1 ].flags = 0;
  appOptions[ 1 ].arg = 0;
  appOptions[ 1 ].arg_data = NULL;
  appOptions[ 1 ].description = NULL; 
  appOptions[ 1 ].arg_description = NULL;

  g_application_add_main_option_entries( G_APPLICATION( app ), appOptions );

  return app;
}

