
#ifndef FredWorksMembershipApp_h_
#define FredWorksMembershipApp_h_

#include <gtk/gtk.h>

#define FREDWORKS_MEMBERSHIP_APP_TYPE ( fredworks_membership_app_get_type( ) )
G_DECLARE_FINAL_TYPE( FredWorksMembershipApp, fredworks_membership_app, FREDWORKS_MEMBERSHIP, APP, GtkApplication)

FredWorksMembershipApp *
fredworks_membership_app_new( void );

const char *
fredworks_membership_app_get_key_store_file( FredWorksMembershipApp * app );

#endif

