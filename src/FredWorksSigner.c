
#include "FredWorksHasher.h"
#include <stdlib.h>
#include <string.h>

// Using openwall.net's bcrypt implementation
#define __SKIP_GNU
#include <ow-crypt.h>
#undef __SKIP_GNU

#include <errno.h>

// TODO debug only
#include <stdio.h>

// This needs to be adjusted manually, but shoot for 5 seconds on a Raspberry Pi 3B+
// bcrypt
#define FREDWORKS_MEMBERSHIP_ENCRYPTION_PREFIX "$2b$"
// 2^12 iterations
#define FREDWORKS_MEMBERSHIP_ENCRYPTION_ITERATION_EXPONENT 12
// 128 bits, 16 bytes
#define FREDWORKS_MEMBERSHIP_SALT_SIZE 16

void
fredworks_hasher_reset( FredWorksHasher * hasher )
{
  hasher->cursor = 0;
  for( char i = 0; i < FREDWORKS_HASHER_BLOB_LIMIT; ++i )
  {
    hasher->blob[ i ] = 0;
  }
}

void
fredworks_hasher_feed( FredWorksHasher * hasher, const void * bytes, int size )
{
  for( char i = 0; i < size; ++i )
  {
    hasher->blob[ hasher->cursor ] ^= ( (const char *) bytes )[ i ];
    ++hasher->cursor;
    if( hasher->cursor == FREDWORKS_HASHER_BLOB_LIMIT )
    {
      hasher->cursor = 0;
    }
  }
}

static void
generate_salt( char * buffer, size_t size )
{
  // Read values from urandom
  FILE * f = fopen( "/dev/urandom", "r" );
  fread( ( void * ) buffer, 1, size, f );
  fclose( f );
}

void
fredworks_hasher_hash( const FredWorksHasher * hasher, char ** result, int * result_size )
{
  // Is the hash length unknown?
  //
  // Actually bcrypt the blob
  char salt[ 16 ];
  generate_salt( salt, FREDWORKS_MEMBERSHIP_SALT_SIZE );

  char * gensalt_result;
  gensalt_result = crypt_gensalt_ra(
    FREDWORKS_MEMBERSHIP_ENCRYPTION_PREFIX,
    FREDWORKS_MEMBERSHIP_ENCRYPTION_ITERATION_EXPONENT,
    salt,
    FREDWORKS_MEMBERSHIP_SALT_SIZE );
  if( gensalt_result == NULL )
  {
    *result = NULL;
    *result_size = 0;
    return;
  }

  // point "result" to NULL
  * result = NULL;
  crypt_ra( hasher->blob, gensalt_result, (void **) result, result_size );
  // omit the trailing nil from the result size
  *result_size -= 1;
  free( gensalt_result );
}

int
fredworks_hasher_verify( const FredWorksHasher * hasher, const char * original, int original_size )
{
  // Use "original" as the settings
  char * result = NULL;
  int result_size;
  result = crypt_ra( hasher->blob, original, (void **) &result, &result_size );
  result_size -= 1;
  if( result == NULL )
  {
    return 0;
  }
  if( original_size != result_size )
  {
    free( result );
    return 0;
  }
  const char * original_arr = original;
  for( int i = 0; i < original_size; i++ )
  {
    if( original_arr[ i ] != result[ i ] )
    {
      free( result );
      return 0;
    }
  }
  free( result );
  return 1;
}


