
#ifndef FredWorksScanCardWindow_h_
#define FredWorksScanCardWindow_h_

#include <gtk/gtk.h>

#define FREDWORKS_SCAN_CARD_WINDOW_TYPE ( fredworks_scan_card_window_get_type( ) )
G_DECLARE_FINAL_TYPE( FredWorksScanCardWindow, fredworks_scan_card_window, FREDWORKS_SCAN_CARD, WINDOW, GtkWindow )

FredWorksScanCardWindow * fredworks_scan_card_window_new( const char * fingerprint );

#endif

