
#ifndef FredWorksBarcodeData_h
#define FredWorksBarcodeData_h

#include <stdio.h>

typedef struct FredWorksBarcodeData_ FredWorksBarcodeData;

struct FredWorksBarcodeData_;

FredWorksBarcodeData * fredworks_barcode_data_new(
  const char * member_name,
  const char * expiration_date );

FredWorksBarcodeData * fredworks_barcode_data_new_from_pack(
  const char * pack );

// Implemented
int fredworks_barcode_data_use_private_key( FredWorksBarcodeData * data, const char * fingerprint );

// Returns a string pointer containing the key's fingerprint that must be free'd
char *
fredworks_barcode_data_add_public_key( const void * pubkey_pointer, size_t pubkey_size );

void fredworks_barcode_data_free(
  FredWorksBarcodeData * data );

const char * fredworks_barcode_data_member_name(
  const FredWorksBarcodeData * data );

const char * fredworks_barcode_data_expiration_date(
  const FredWorksBarcodeData * data );

int fredworks_barcode_data_pack_size(
  FredWorksBarcodeData * data );

void fredworks_barcode_data_to_pack(
  FredWorksBarcodeData * data,
  char * pack );

// TODO Implement
int fredworks_barcode_data_check(
  const FredWorksBarcodeData * data );

#endif

