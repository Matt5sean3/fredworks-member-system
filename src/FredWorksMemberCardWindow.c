
#include "FredWorksMemberCardWindow.h"
#include "FredWorksBarcodeData.h"
#include <cairo/cairo.h>

#include <stdio.h>
#include <string.h>

// How we make barcodes
#include <zint.h>

struct _FredWorksMemberCardWindow
{
  GtkWindow parent;
};

typedef struct _FredWorksMemberCardWindowPrivate FredWorksMemberCardWindowPrivate;

struct _FredWorksMemberCardWindowPrivate
{
  GtkFileFilter * zint_save_filter;

  cairo_surface_t * surface;
  cairo_surface_t * barcode_surface;
  struct zint_symbol * barcode;

  int base_width;
  int base_height;
  int window_width;
  int window_height;

  char * memberName;
  char * expirationDateStr;
};

G_DEFINE_TYPE_WITH_PRIVATE( FredWorksMemberCardWindow, fredworks_member_card_window, GTK_TYPE_WINDOW );

// Forward definitions
void
fredworks_member_card_window_render( FredWorksMemberCardWindow * win );

/* === BEGIN UTILITY FUNCTIONS === */
static FredWorksMemberCardWindow *
fredworks_member_card_window_from_child( GtkWidget * widget )
{
  GtkWidget * topLevelWidget = gtk_widget_get_toplevel( widget );
  return G_OBJECT_TYPE( topLevelWidget ) == FREDWORKS_MEMBER_CARD_WINDOW_TYPE ?
    FREDWORKS_MEMBER_CARD_WINDOW( topLevelWidget ) :
    NULL;
}
/* === END UTILITY FUNCTIONS === */

/* === START CALLBACKS === */
static gboolean
member_card_drawing_area_configure_event_cb(
  GtkWidget * widget,
  GdkEvent * event,
  gpointer user_data)
{
  FredWorksMemberCardWindow * win = fredworks_member_card_window_from_child( widget );
  if( win == NULL )
  {
    return FALSE;
  }
  FredWorksMemberCardWindowPrivate * priv = fredworks_member_card_window_get_instance_private( win );
  if( priv->surface )
  {
    // Destroy any existing cairo surface
    cairo_surface_destroy( priv->surface );
  }
  GdkWindow * barcode_window = gtk_widget_get_window( widget );
  priv->surface = gdk_window_create_similar_surface(
    barcode_window,
    CAIRO_CONTENT_COLOR,
    gtk_widget_get_allocated_width( widget ),
    gtk_widget_get_allocated_height( widget ) );

  priv->window_height = gdk_window_get_height( barcode_window );
  priv->window_width = gdk_window_get_width( barcode_window );

  fredworks_member_card_window_render( win );

  cairo_t * c = cairo_create( priv->surface );

  cairo_set_source_rgb( c, 1, 1, 1 );
  cairo_paint( c );

  if( priv->barcode_surface )
  {
    cairo_set_source_surface( c, priv->barcode_surface, 5.0, 5.0 );
  }
  cairo_paint( c );

  float line_height = (float) priv->window_height / ( 10.0f );
  cairo_text_extents_t text_measurement;

  // Credit cards have 3.370 x 2.125 in, this is 3.370 x 4.250 in unfolded card, half of which is the barcode
  float scaleHeight = (float) priv->window_height / 4.250f;
  float scaleWidth = (float) priv->window_width / 3.370f;
  float scale = scaleHeight > scaleWidth ? scaleWidth : scaleHeight;

  cairo_set_source_rgb( c, 0, 0, 0 );

  cairo_identity_matrix( c );
  cairo_scale( c, scale, scale );
  cairo_new_path( c );
  cairo_translate( c, 0.0f, 2.125f );
  cairo_translate( c, 0.25f, 0.5f );
  cairo_set_font_size( c, 0.25 );
  cairo_text_path( c, "Member: " );
  cairo_text_extents( c, "Member: ", &text_measurement );
  cairo_translate( c, text_measurement.x_advance, 0.0 );
  cairo_text_path( c, priv->memberName );
  cairo_fill( c );

  cairo_identity_matrix( c );
  cairo_scale( c, scale, scale );
  cairo_new_path( c );
  cairo_translate( c, 0.0f, 2.125f );
  cairo_translate( c, 0.25f, 1.5f );
  cairo_text_path( c, "Expiration: " );
  cairo_text_extents( c, "Expiration: ", &text_measurement );
  cairo_translate( c, text_measurement.x_advance, 0.0 );
  cairo_text_path( c, priv->expirationDateStr );
  cairo_fill( c );

  cairo_destroy( c );

  return TRUE;
}

static gboolean
member_card_drawing_area_draw_cb(
  GtkWidget * widget,
  cairo_t * cr,
  gpointer user_data )
{
  FredWorksMemberCardWindow * win = fredworks_member_card_window_from_child( widget );
  if( win == NULL )
  {
    return FALSE;
  }
  FredWorksMemberCardWindowPrivate * priv = fredworks_member_card_window_get_instance_private( win );
  // redraw a surface we already have
  if( priv->surface )
  {
    cairo_set_source_surface( cr, priv->surface, 0, 0 );
    cairo_paint( cr );
  }
  else
  {
    printf( "UNABLE TO DRAW, BACK BUFFER MISSING!\n" );
  }

  return FALSE;
}

static void
fredworks_member_card_window_destroy_cb(
  GtkWidget * widget,
  gpointer userdata)
{
  FredWorksMemberCardWindow * win = fredworks_member_card_window_from_child( widget );
  if( win == NULL )
  {
    return;
  }
  FredWorksMemberCardWindowPrivate * priv = fredworks_member_card_window_get_instance_private( win );
  if( priv->surface )
  {
    cairo_surface_destroy( priv->surface );
    priv->surface = NULL;
  }
  if( priv->barcode_surface )
  {
    cairo_surface_destroy( priv->barcode_surface );
    priv->barcode_surface = NULL;
  }
  if( priv->barcode )
  {
    ZBarcode_Delete( priv->barcode );
    priv->barcode = NULL;
  }
  if( priv->memberName )
  {
    free( priv->memberName );
  }
  if( priv->expirationDateStr )
  {
    free( priv->expirationDateStr );
  }
}

static void
close_button_clicked_cb(
  GtkButton * button,
  gpointer userdata)
{
  FredWorksMemberCardWindow * win = fredworks_member_card_window_from_child( GTK_WIDGET( button ) );
  if( win == NULL )
  {
    return;
  }
  gtk_widget_destroy( GTK_WIDGET( win ) );
}

static void
save_button_clicked_cb(
  GtkButton * widget,
  gpointer user_data )
{
  FredWorksMemberCardWindow * win = fredworks_member_card_window_from_child( GTK_WIDGET( widget ) );
  if( win == NULL )
  {
    return;
  }
  FredWorksMemberCardWindowPrivate * priv = fredworks_member_card_window_get_instance_private( win );
  gint res;

  // Generate a new save dialog
  GtkWidget * save_widget = gtk_file_chooser_dialog_new(
    "Save Member Card",
    GTK_WINDOW( win ),
    GTK_FILE_CHOOSER_ACTION_SAVE,
    "Cancel",
    GTK_RESPONSE_CANCEL,
    "Save",
    GTK_RESPONSE_ACCEPT,
    NULL );

  GtkFileChooser * save_dialog = GTK_FILE_CHOOSER( save_widget );

  // limit to SVG, PNG, or PostScript
  gtk_file_chooser_set_filter( save_dialog, priv->zint_save_filter );

  // Can uncomment to save barcode to file for debugging purposes
  res = gtk_dialog_run( GTK_DIALOG( save_dialog ) );
  if( res == GTK_RESPONSE_ACCEPT )
  {
    char * barcode_path;
    barcode_path = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER( save_dialog ) );
    memcpy( priv->barcode->outfile, barcode_path, strlen( barcode_path ) + 1 );
    ZBarcode_Print( priv->barcode, 0 );
    g_free( barcode_path );
  }
  gtk_widget_destroy( save_widget );
}

/* === END CALLBACKS === */

static void
fredworks_member_card_window_init( FredWorksMemberCardWindow * win )
{
  gtk_widget_init_template( GTK_WIDGET( win ) );
  FredWorksMemberCardWindowPrivate * priv = fredworks_member_card_window_get_instance_private( win );
  priv->surface = NULL;
  priv->barcode_surface = NULL;
  priv->barcode = NULL;
  priv->memberName = NULL;
  priv->expirationDateStr = NULL;
}

static void
fredworks_member_card_window_class_init( FredWorksMemberCardWindowClass * class )
{
  gtk_widget_class_set_template_from_resource(
    GTK_WIDGET_CLASS( class ),
    "/org/fwks/membership/resources/gui/FredWorksMemberCardWindow.glade" );
  // bind children
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksMemberCardWindow,
    zint_save_filter );
  // bind callbacks
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    member_card_drawing_area_configure_event_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    member_card_drawing_area_draw_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    fredworks_member_card_window_destroy_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    close_button_clicked_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    save_button_clicked_cb );
}

FredWorksMemberCardWindow *
fredworks_member_card_window_new(
  const gchar * memberName,
  gint expiration,
  const char * fingerprint )
{
  FredWorksMemberCardWindow * win = g_object_new(
    FREDWORKS_MEMBER_CARD_WINDOW_TYPE,
    NULL );
  FredWorksMemberCardWindowPrivate * priv =
    fredworks_member_card_window_get_instance_private( win );
  fredworks_member_card_window_update(
    win,
    memberName,
    expiration,
    fingerprint );
  return win;
} 

void
fredworks_member_card_window_render( FredWorksMemberCardWindow * win )
{
  FredWorksMemberCardWindowPrivate * priv =
    fredworks_member_card_window_get_instance_private( win );

  // Credit cards have 3.370 x 2.125 in, this is 3.370 x 4.250 in unfolded card, half of which is the barcode
  // Folding the card over should protect the ink without needing lamination
  float scaleHeight = (float) priv->window_height / 4.250f;
  float scaleWidth = (float) priv->window_width / 3.370f;
  // needs to scale base_width to 3.0
  priv->barcode->scale = ( scaleHeight > scaleWidth ? scaleWidth : scaleHeight ) * ( 3.0f / ( 2.0f * (float) priv->base_width ) );

  ZBarcode_Buffer( priv->barcode, 0 );

  if( priv->barcode_surface )
  {
    cairo_surface_destroy( priv->barcode_surface );
  }

  // TODO check for failures
  priv->barcode_surface = cairo_image_surface_create(
    CAIRO_FORMAT_RGB24,
    priv->barcode->bitmap_width,
    priv->barcode->bitmap_height );

  unsigned char * image_data = cairo_image_surface_get_data( priv->barcode_surface );
  // 4 bytes per pixel
  // Populate with bitmap data
  for( int i = 0; i < priv->barcode->bitmap_height * priv->barcode->bitmap_width; i++ )
  { // Really direct 3 bytes per pixel to 4 bytes per pixel mapping
    // Endianness might come up
    uint32_t * bitmap_pixel = ( uint32_t * ) &priv->barcode->bitmap[ i * 3 ];
    uint32_t * image_pixel = ( uint32_t * ) &image_data[ i * 4 ];
    // this should work on little endian
    *image_pixel = *bitmap_pixel & 0x00FFFFFF;
  }

}

void
fredworks_member_card_window_update(
  FredWorksMemberCardWindow * win,
  const gchar * memberName, 
  gint expiration,
  const char * fingerprint )
{
  FredWorksMemberCardWindowPrivate * priv =
    fredworks_member_card_window_get_instance_private( win );

  // TODO duplicates code in "update_expiration_label" in FredWorksMembershipAppWindow.c
  GDateTime * now;
  GDateTime * expiration_date;
  now = g_date_time_new_now_local( );
  expiration_date = g_date_time_add_months( now, expiration );
  g_date_time_unref( now );
  gchar * date_str = g_date_time_format(
    expiration_date,
    "%Y-%m-%d" );
  g_date_time_unref( expiration_date );

  priv->memberName = strdup( memberName );
  priv->expirationDateStr = strdup( date_str );

  FredWorksBarcodeData * barcode_data;
  barcode_data = fredworks_barcode_data_new( memberName, date_str );
  g_free( date_str );

  int pack_size = fredworks_barcode_data_pack_size( barcode_data );
  char pack[ pack_size ];
  fredworks_barcode_data_to_pack( barcode_data, pack );
  fredworks_barcode_data_free( barcode_data );

  if( priv->barcode )
  {
    ZBarcode_Delete( priv->barcode );
  }
  priv->barcode = ZBarcode_Create( );
  priv->barcode->symbology = BARCODE_AZTEC;
  priv->barcode->option_1 = 3; // Pretty good error correction 36%
  ZBarcode_Encode( priv->barcode, pack, pack_size );
  priv->barcode->scale = 1.0;
  ZBarcode_Buffer( priv->barcode, 0 );
  // Unscaled barcode dimensions
  priv->base_width = priv->barcode->bitmap_width;
  priv->base_height = priv->barcode->bitmap_height;
}

