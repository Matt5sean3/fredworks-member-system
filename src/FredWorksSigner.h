
#ifndef FredWorksHasher_h_
#define FredWorksHasher_h_

// bcrypt has a key length limit of 72
#define FREDWORKS_HASHER_BLOB_LIMIT 72

typedef struct _FredWorksHasher FredWorksHasher;

struct _FredWorksHasher
{
  char blob[ FREDWORKS_HASHER_BLOB_LIMIT ];
  char cursor;
};

void
fredworks_hasher_reset( FredWorksHasher * hasher );

void
fredworks_hasher_feed( FredWorksHasher * hasher, const void * bytes, int size );

void
fredworks_hasher_hash( const FredWorksHasher * hasher, char ** result, int * result_size );

int
fredworks_hasher_verify( const FredWorksHasher * hasher, const char * original, int original_size );

#endif

