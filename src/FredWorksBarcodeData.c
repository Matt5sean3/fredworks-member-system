
#include "FredWorksBarcodeData.h"
#include <gpgme.h>
#include <string.h>
#include <stdlib.h>

struct FredWorksBarcodeData_
{
  gpgme_ctx_t ctx;
  int member_name_size;
  char * member_name;
  int expiration_date_size;
  char * expiration_date;
  int signature_size;
  char * signature;

  int dirty;
};

// Return true in cases of failure
static int
initialize_gpgme( void )
{
  static int initialized = 0;

  if( initialized )
  {
    return 0;
  }

  gpgme_error_t err;

  const char * version;
  if( ( version = gpgme_check_version( "1.13.1" ) ) == NULL )
  {
    printf( "Minimum OpenPGP Engine version not met\n" );
    return 1;
  }
  else
  {
    printf( "Using GPGME Version: %s\n", version );
  }

  if( ( err = gpgme_engine_check_version( GPGME_PROTOCOL_OpenPGP ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "OpenPGP engine not available\n" );
    return 1;
  }

  initialized = 1;

  return 0;
}

static gpgme_ctx_t
create_gpgme_ctx( void )
{
  if( initialize_gpgme( ) )
  {
    return NULL;
  }

  gpgme_error_t err;
  gpgme_ctx_t ctx;

  // Perform multiple operations
  if(
    // Create the context
    ( err = gpgme_new( &ctx ) ) != GPG_ERR_NO_ERROR ||
    // We shouldn't need a custom mechanism for pin entry, so use the default
    ( err = gpgme_set_pinentry_mode( ctx, GPGME_PINENTRY_MODE_ASK ) ) != GPG_ERR_NO_ERROR ||
    // Only need local keys
    ( err = gpgme_set_keylist_mode( ctx, GPGME_KEYLIST_MODE_LOCAL ) ) != GPG_ERR_NO_ERROR ||
    // Using OpenPGP backend
    ( err = gpgme_set_protocol( ctx, GPGME_PROTOCOL_OPENPGP ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME ERROR: %s\n", gpgme_strerror( err ) );
    return NULL;
  }

  // Enable offline mode, we're not checking certificates anyway
  gpgme_set_offline( ctx, 1 );
  // Disable ASCII armor
  gpgme_set_armor( ctx, 0 );

  return ctx;
}

FredWorksBarcodeData *
fredworks_barcode_data_new(
  const char * member_name,
  const char * expiration_date )
{
  FredWorksBarcodeData * data = malloc( sizeof( FredWorksBarcodeData ) );
  data->member_name_size = strlen( member_name );
  data->member_name = strdup( member_name );
  data->expiration_date_size = strlen( expiration_date );
  data->expiration_date = strdup( expiration_date );
  data->signature_size = 0;
  data->signature = 0;
  data->ctx = create_gpgme_ctx( );
  data->dirty = 1;
  return data;
}

static int unpack_string(
  const char * source,
  int * length,
  char ** contents )
{
  if(
   source[ 0 ] >= '0' && source[ 0 ] <= '9' &&
   source[ 1 ] >= '0' && source[ 1 ] <= '9' &&
   source[ 2 ] >= '0' && source[ 2 ] <= '9' )
  {
    *length = ( source[ 0 ] - '0' ) * 100 + ( source[ 1 ] - '0' ) * 10 + source[ 2 ] - '0';
    *contents = memcpy( malloc( * length ), &source[ 3 ], * length ); // Appends a \0, so safe for strcmp
    return 0;
  }
  else
  {
    return 1;
  }
}

FredWorksBarcodeData *
fredworks_barcode_data_new_from_pack( const char * pack )
{
  FredWorksBarcodeData * data = malloc( sizeof( FredWorksBarcodeData ) );
  data->member_name_size = 0;
  data->expiration_date_size = 0;
  data->signature_size = 0;
  if(
    unpack_string( pack, &data->member_name_size, &data->member_name ) ||
    unpack_string( &pack[ 3 + data->member_name_size ], &data->expiration_date_size, &data->expiration_date ) ||
    unpack_string( &pack[ 6 + data->member_name_size + data->expiration_date_size ], &data->signature_size, &data->signature ) )
  {
    fredworks_barcode_data_free( data );
    return NULL;
  }
  data->ctx = create_gpgme_ctx( );
  data->dirty = 1;
  return data;
}

void
fredworks_barcode_data_free(
  FredWorksBarcodeData * barcode_data )
{
  if( barcode_data->member_name_size )
  {
    free( barcode_data->member_name );
  }
  if( barcode_data->expiration_date_size )
  {
    free( barcode_data->expiration_date );
  }
  if( barcode_data->signature_size )
  {
    free( barcode_data->signature );
  }
}

const char * fredworks_barcode_data_member_name(
  const FredWorksBarcodeData * data )
{
  return data->member_name;
}

const char * fredworks_barcode_data_expiration_date(
  const FredWorksBarcodeData * data )
{
  return data->expiration_date;
}

static void pack_string(
  char * destination,
  int size,
  const char * contents )
{
  destination[ 0 ] = '0' + ( size / 100 ) % 10;
  destination[ 1 ] = '0' + ( size / 10 ) % 10;
  destination[ 2 ] = '0' + size % 10;
  memcpy( &destination[ 3 ], contents, size );
}

int fredworks_barcode_data_update_signature(
  FredWorksBarcodeData * data )
{
  if( data ==  NULL ||
    data->member_name == NULL ||
    data->expiration_date == NULL )
  { // Cannot proceed, not enough data members set
    printf( "Cannot update signature, not enough information\n" );
    return 1;
  }

  if( ! data->dirty )
  {
    return 0;
  }

  int result_size;

  size_t signed_data_size = 4 + data->member_name_size + data->expiration_date_size;
  char * signed_data = malloc( signed_data_size );

  pack_string( signed_data, data->member_name_size, data->member_name );
  pack_string( &signed_data[ 3 + data->member_name_size ], data->expiration_date_size, data->expiration_date );

  gpgme_data_t signed_data_obj;
  gpgme_data_new_from_mem( &signed_data_obj, signed_data, signed_data_size, 0 );

  gpgme_data_t signature_data_obj;
  gpgme_data_new( &signature_data_obj );
  gpgme_data_set_encoding( signature_data_obj, GPGME_DATA_ENCODING_BINARY );

  printf( "Updating signature with %i signer(s).\n", gpgme_signers_count( data->ctx ) );

  gpgme_error_t err;
  // Create a detached signature
  if( ( err = gpgme_op_sign( data->ctx, signed_data_obj, signature_data_obj, GPGME_SIG_MODE_DETACH ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME ERROR: failed signing (%s)\n", gpgme_strerror( err ) );
    return 1;
  }

  gpgme_data_release( signed_data_obj );
  free( signed_data );

  // Retrieve the fingerprint used for the signature
  gpgme_sign_result_t signature_result = gpgme_op_sign_result( data->ctx );
  for( gpgme_new_signature_t signature = signature_result->signatures; signature != NULL; signature = signature->next )
  {
    printf( "Responsible fingerprint: %s\n", signature->fpr );
  }

  size_t signature_length;
  unsigned char * signature_data = gpgme_data_release_and_get_mem( signature_data_obj, &signature_length );

  // TODO handle out of memory condition
  data->signature = memcpy( malloc( signature_length ), signature_data, signature_length );
  data->signature_size = signature_length;

  gpgme_free( signature_data );

  // Print the resulting signature
  printf( "Signature size: %i\n", data->signature_size );
  for( size_t i = 0; i < data->signature_size; i++ )
  {
    if( i % 8 == 0 )
      printf( "\n" );
    printf( "%2x", (unsigned char) data->signature[i] );
  }
  printf( "\n\n" );


  data->dirty = 0;
  // Go through detailed results
  return 0;
} 

int fredworks_barcode_data_pack_size(
  FredWorksBarcodeData * data )
{
  // Ensure the signature is up to date for length purposes
  fredworks_barcode_data_update_signature( data );
  return data->member_name_size + data->expiration_date_size + data->signature_size + 9;
}

void fredworks_barcode_data_to_pack(
  FredWorksBarcodeData * data,
  char * destination )
{
  // Ensure the signature is up to date
  fredworks_barcode_data_update_signature( data );

  pack_string( destination, data->member_name_size, data->member_name );
  pack_string( &destination[ 3 + data->member_name_size ], data->expiration_date_size, data->expiration_date );
  // Binary data is fun
  pack_string( &destination[ 6 + data->member_name_size + data->expiration_date_size ], data->signature_size, data->signature );
}

int fredworks_barcode_data_check(
  const FredWorksBarcodeData * data )
{
  if( data ==  NULL ||
    data->member_name == NULL ||
    data->expiration_date == NULL )
  { // Cannot proceed, not enough data members set
    return 0;
  }

  gpgme_error_t err;

  // Print out the raw signature
  printf( "Signature size: %i\n", data->signature_size );
  for( size_t i = 0; i < data->signature_size; i++ )
  {
    if( i % 8 == 0 )
      printf( "\n" );
    printf( "%2x", (unsigned char) data->signature[i] );
  }
  printf( "\n\n" );

  // Verify the signature
  gpgme_data_t signature_data_obj;
  gpgme_data_new_from_mem( &signature_data_obj, data->signature, data->signature_size, 0 );

  size_t signed_data_size = 4 + data->member_name_size + data->expiration_date_size;
  char * signed_data = malloc( signed_data_size );
  pack_string( signed_data, data->member_name_size, data->member_name );
  pack_string( &signed_data[ 3 + data->member_name_size ], data->expiration_date_size, data->expiration_date );

  gpgme_data_t signed_data_obj;
  gpgme_data_new_from_mem( &signed_data_obj, signed_data, signed_data_size, 0 );

  err = gpgme_op_verify( data->ctx, signature_data_obj, signed_data_obj, NULL );
  if( err != GPG_ERR_NO_ERROR )
  { // Failed during verification
    return 0;
  }

  
  int valid = 0;
  for( gpgme_signature_t result = gpgme_op_verify_result( data->ctx )->signatures;
      result != NULL;
      result = result->next )
  {
    printf( "Responsible fingerprint: %s\n", result->fpr );
    if( result->summary & GPGME_SIGSUM_VALID )
    {
      valid = 1;
      break;
    }
  }

  gpgme_data_release( signed_data_obj );
  gpgme_data_release( signature_data_obj );
  free( signed_data );

  return valid;
}

static void
load_public_key_try_import_key( gpgme_ctx_t ctx, gpgme_key_t public_key )
{
  gpgme_error_t err;
  int on_keyring = 0;
  // Check the keyring for the public key
  if( ( err = gpgme_op_keylist_start( ctx, "members@fwks.org", 0 ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME error while opening keyring: %s\n", gpgme_strerror( err ) );
  }

  int alreadyLoaded = 0;
  gpgme_key_t key;
  while( ( err = gpgme_op_keylist_next( ctx, &key ) ) == GPG_ERR_NO_ERROR )
  {
    // Look through the subkeys for one that signs and supports GPGME_PK_EDDSA
    gpgme_subkey_t subkey;
    for( subkey = key->subkeys; subkey; subkey = subkey->next )
    {
      if( strcmp( public_key->fpr, subkey->fpr ) == 0 )
      {
        alreadyLoaded = 1;
        gpgme_op_keylist_end( ctx );
      }
    }
    gpgme_key_release( key );
    if( alreadyLoaded )
    {
      return;
    }
  }

  gpgme_key_t importing_keys[ 2 ];
  importing_keys[ 0 ] = public_key;
  importing_keys[ 1 ] = NULL;
  if( ! alreadyLoaded )
  {
    // import the key
    gpgme_op_import_keys( ctx, importing_keys );
  }
}

static char *
load_public_key_get_fingerprint( gpgme_ctx_t ctx, gpgme_data_t public_key_data )
{
  gpgme_error_t err;
  if( ( err = gpgme_op_keylist_from_data_start( ctx, public_key_data, 0 ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME error starting listing public key data: %s\n", gpgme_strerror( err ) );
    return NULL;
  }

  char * fingerprint;
  gpgme_key_t public_key;
  if( ctx == NULL )
  {
    printf( "ctx is null\n" );
  }
  err = gpgme_op_keylist_next( ctx, &public_key );
  if( err != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME error while listing public key data: %s\n", gpgme_strerror( err ) );
    return NULL;
  }
  // Get the public key fingerprint
  fingerprint = strdup( public_key->subkeys->fpr );

  gpgme_key_release( public_key );
  return fingerprint;
}

char *
fredworks_barcode_data_add_public_key( const void * pubkey_pointer, size_t pubkey_size )
{
  gpgme_ctx_t ctx = create_gpgme_ctx( );

  gpgme_error_t err;
  gpgme_data_t public_key_data = NULL;
  if( ( err = gpgme_data_new_from_mem( &public_key_data, ( (const char *) pubkey_pointer ), pubkey_size, 0 ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME error while loading public key data: %s\n", gpgme_strerror( err ) );
    return NULL;
  }
  char * fingerprint = load_public_key_get_fingerprint( ctx, public_key_data );

  gpgme_data_release( public_key_data );
  gpgme_release( ctx );

  return fingerprint;
}

static int
set_signing_key_find_key( gpgme_ctx_t ctx, const char * fingerprint )
{
  gpgme_error_t err;
  gpgme_key_t private_key;
  while( ( err = gpgme_op_keylist_next( ctx, &private_key ) ) == GPG_ERR_NO_ERROR )
  {
    // Match up the fingerprint and check that it can sign
    if( strcmp( fingerprint, private_key->fpr ) == 0 && private_key->can_sign )
    {
      gpgme_signers_add( ctx, private_key );
      gpgme_op_keylist_end( ctx );
      gpgme_key_release( private_key );
      return 1;
    }
    gpgme_key_release( private_key );
  }
  return 0;
}

int
fredworks_barcode_data_use_private_key( FredWorksBarcodeData * data, const char * fingerprint )
{
  data->dirty = 1;
  gpgme_error_t err;
  gpgme_signers_clear( data->ctx );
  if( ( err = gpgme_op_keylist_start( data->ctx, fingerprint, 1 ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "GPGME ERROR: %s\n", gpgme_strerror( err ) );
    return 0;
  }
  return set_signing_key_find_key( data->ctx, fingerprint );
}

static int 
retrieve_keys( const void * pubkey_pointer, size_t pubkey_size, FILE * f )
{
  gpgme_ctx_t ctx = create_gpgme_ctx( );

}

