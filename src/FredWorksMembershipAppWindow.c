
#include "FredWorksMembershipAppWindow.h"
#include "FredWorksMemberCardWindow.h"
#include "FredWorksScanCardWindow.h"
#include "FredWorksBarcodeData.h"
#include <stdio.h>

struct _FredWorksMembershipAppWindow
{
  GtkApplicationWindow parent;
};

typedef struct _FredWorksMembershipAppWindowPrivate FredWorksMembershipAppWindowPrivate;

struct _FredWorksMembershipAppWindowPrivate
{
  GtkEntryBuffer * member_name_buffer;
  GtkAdjustment * expiration_in_months_adjustment;
  GtkLabel * expiration_date_label;
  GtkButton * generate_button;

  GWeakRef member_card_window; // FredWorksMemberCardWindow
  gboolean have_member_card_window;

  GWeakRef scan_card_window; // FredWorksScanCardWindow
  gboolean have_scan_card_window;

  guint32 generate_button_release_time;
  guint32 scan_card_button_release_time;

  gboolean have_signing_key;
  char * fingerprint;
};

G_DEFINE_TYPE_WITH_PRIVATE( FredWorksMembershipAppWindow, fredworks_membership_app_window, GTK_TYPE_APPLICATION_WINDOW );

/* === START UTILITY FUNCTIONS === */
static FredWorksMembershipAppWindow *
fredworks_membership_app_window_from_child( GtkWidget * widget )
{
  GtkWidget * topLevelWidget = gtk_widget_get_toplevel( widget );
  return G_OBJECT_TYPE( topLevelWidget ) == FREDWORKS_MEMBERSHIP_APP_WINDOW_TYPE ?
    FREDWORKS_MEMBERSHIP_APP_WINDOW( topLevelWidget ) :
    NULL;
}

static void
update_expiration_label(
  GtkLabel * expiration_label,
  GtkAdjustment * adjustment )
{
  GDateTime * now;
  GDateTime * expiration;
  now = g_date_time_new_now_local( );
  expiration = g_date_time_add_months( now, (gint) gtk_adjustment_get_value(adjustment) );
  g_date_time_unref( now );
  gchar * date_str = g_date_time_format(
    expiration, "%Y-%m-%d" );
  g_date_time_unref( expiration );
  gtk_label_set_text(
    expiration_label,
    date_str );
  g_free( date_str );
}
/* === END UTILITY FUNCTIONS === */

/* === START CALLBACKS === */

static void
generate_button_clicked_cb( GtkButton * widget, gpointer userdata )
{
  FredWorksMembershipAppWindow * win = fredworks_membership_app_window_from_child( GTK_WIDGET( widget ) );
  if( ! win )
  {
    return;
  }
  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );

  // Open the member card window
  FredWorksMemberCardWindow * member_card_window;

  // Helps with thread safety
  if( priv->have_member_card_window &&
    ( member_card_window = g_weak_ref_get( &priv->member_card_window ) ) != NULL )
  {
    // Update the existing member card window
    fredworks_member_card_window_update(
      member_card_window,
      gtk_entry_buffer_get_text( priv->member_name_buffer ),
      gtk_adjustment_get_value( priv->expiration_in_months_adjustment ),
      priv->fingerprint );

    gtk_window_present_with_time(
      GTK_WINDOW( member_card_window ),
      priv->generate_button_release_time );

    // Free the temporary reference
    g_object_unref( member_card_window );
  }
  else
  {
    // Create a new member card window
    member_card_window =
      fredworks_member_card_window_new(
        gtk_entry_buffer_get_text( priv->member_name_buffer ),
        gtk_adjustment_get_value( priv->expiration_in_months_adjustment ),
        priv->fingerprint );

    if( priv->have_member_card_window )
    {
      g_weak_ref_set(
        &priv->member_card_window,
        member_card_window );
    }
    else
    {
      priv->have_member_card_window = TRUE;
      g_weak_ref_init(
        &priv->member_card_window,
        member_card_window );
    }
    gtk_widget_show_all( GTK_WIDGET( member_card_window ) );
  }
}

static void
scan_card_button_clicked_cb( GtkButton * widget, gpointer userdata )
{
  FredWorksMembershipAppWindow * win = fredworks_membership_app_window_from_child( GTK_WIDGET( widget ) );
  if( ! win )
  {
    return;
  }
  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );

  // TODO open the card scanning dialog or bring the existing one to the front
  FredWorksScanCardWindow * scan_card_window;
  if( priv->have_scan_card_window && ( scan_card_window = g_weak_ref_get( &priv->scan_card_window ) ) != NULL )
  { // Bring existing window to the front
    gtk_window_present_with_time(
      GTK_WINDOW( scan_card_window ),
      priv->scan_card_button_release_time );
    g_object_unref( scan_card_window );
  }
  else
  {
    scan_card_window = fredworks_scan_card_window_new( priv->fingerprint );
    if( priv->have_scan_card_window )
    { // Replace the weak ref
      g_weak_ref_set(
        &priv->scan_card_window,
        scan_card_window );
    }
    else
    { // Initialize the weak ref
      priv->have_scan_card_window = TRUE;
      g_weak_ref_init(
        &priv->scan_card_window,
        scan_card_window );
    }
    gtk_widget_show_all( GTK_WIDGET( scan_card_window ) );
  }
}

static gboolean
generate_button_button_release_event_cb(
  GtkWidget * widget,
  GdkEvent * event,
  gpointer user_data )
{
  // I know this is weird, but it's a way to work out timing
  FredWorksMembershipAppWindow * win = fredworks_membership_app_window_from_child( GTK_WIDGET( widget ) );
  if( ! win )
  {
    return FALSE;
  }
  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );
  priv->generate_button_release_time = gdk_event_get_time( event );
  return FALSE;
}

static gboolean
scan_card_button_button_release_event_cb(
  GtkWidget * widget,
  GdkEvent * event,
  gpointer user_data )
{
  FredWorksMembershipAppWindow * win = fredworks_membership_app_window_from_child( GTK_WIDGET( widget ) );
  if( ! win )
  {
    return FALSE;
  }
  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );
  priv->scan_card_button_release_time = gdk_event_get_time( event );
  return FALSE;
}

static void
expiration_in_months_control_value_changed_cb( GtkSpinButton * widget, gpointer user_data )
{
  // update expiration text
  FredWorksMembershipAppWindow * win = fredworks_membership_app_window_from_child( GTK_WIDGET( widget ) );
  if( ! win )
  {
    return;
  }
  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );
  // Also needs to happen when the label first appears
  update_expiration_label( priv->expiration_date_label, priv->expiration_in_months_adjustment );
}

static void
expiration_date_label_realize_cb( GtkLabel * widget, gpointer user_data )
{
  // update expiration text
  FredWorksMembershipAppWindow * win = fredworks_membership_app_window_from_child( GTK_WIDGET( widget ) );
  if( ! win )
  {
    return;
  }
  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );
  // Also needs to happen when the label first appears
  update_expiration_label( priv->expiration_date_label, priv->expiration_in_months_adjustment );
}

/* === END CALLBACKS === */

static void
fredworks_membership_app_window_init( FredWorksMembershipAppWindow * win )
{
  gtk_widget_init_template( GTK_WIDGET( win ) );

  FredWorksMembershipAppWindowPrivate * priv = fredworks_membership_app_window_get_instance_private( win );
  priv->have_member_card_window = FALSE;
  priv->have_scan_card_window = FALSE;

  // Data allocation gets monstrous
  GError * pubkey_lookup_error;
  GBytes * pubkey_data =
    g_resources_lookup_data(
      "/org/fwks/membership/resources/public-key.gpg",
      G_RESOURCE_LOOKUP_FLAGS_NONE,
      &pubkey_lookup_error );

  // Ensure the public key is on the keyring
  size_t pubkey_size;
  gconstpointer pubkey_pointer = g_bytes_get_data( pubkey_data, &pubkey_size );
  priv->fingerprint = fredworks_barcode_data_add_public_key( pubkey_pointer, pubkey_size );

  g_bytes_unref( pubkey_data );


  // Only check availability here, not actual usage
  FredWorksMembershipApp * app = FREDWORKS_MEMBERSHIP_APP( gtk_window_get_application( GTK_WINDOW( win ) ) );
  const char * keyStore = app ? fredworks_membership_app_get_key_store_file( app ) : NULL;

  FredWorksBarcodeData * barcode_data = fredworks_barcode_data_new( "", "" );
  // Open up a barcode data instance
  // Make the generate button sensitive if there is a private key, insensitive if there isn't
  // TODO make generate button insensitive until the private key is unlocked
  gtk_widget_set_sensitive(
    GTK_WIDGET( priv->generate_button ),
    fredworks_barcode_data_use_private_key( barcode_data, priv->fingerprint ) );
  fredworks_barcode_data_free( barcode_data );
}


static void
fredworks_membership_app_window_class_init( FredWorksMembershipAppWindowClass * class )
{
  gtk_widget_class_set_template_from_resource(
    GTK_WIDGET_CLASS( class ),
    "/org/fwks/membership/resources/gui/FredWorksMembershipAppWindow.glade" );
  // Bind template children
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksMembershipAppWindow,
    member_name_buffer );
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksMembershipAppWindow,
    expiration_date_label);
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksMembershipAppWindow,
    expiration_in_months_adjustment );
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksMembershipAppWindow,
    generate_button );
  // Bind template callbacks
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    generate_button_clicked_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    expiration_in_months_control_value_changed_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    scan_card_button_clicked_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    generate_button_button_release_event_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    expiration_date_label_realize_cb );
}

FredWorksMembershipAppWindow *
fredworks_membership_app_window_new( FredWorksMembershipApp * app )
{
  FredWorksMembershipAppWindow * win = g_object_new(
      FREDWORKS_MEMBERSHIP_APP_WINDOW_TYPE,
      "application", app,
      NULL );

  return win;
}


