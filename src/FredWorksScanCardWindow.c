
#include "FredWorksScanCardWindow.h"
#include "FredWorksBarcodeData.h"
#include <barcode_reader.h>

struct _FredWorksScanCardWindow
{
  GtkWindow parent;
};

typedef struct _FredWorksScanCardWindowPrivate FredWorksScanCardWindowPrivate;

struct _FredWorksScanCardWindowPrivate
{
  Reader * reader;
  const char * fingerprint;

  cairo_surface_t * camera_surface;

  GtkLabel * last_scanned_username;
  GtkLabel * last_scanned_expiration;
};

G_DEFINE_TYPE_WITH_PRIVATE( FredWorksScanCardWindow, fredworks_scan_card_window, GTK_TYPE_WINDOW )

/* === START UTILITY FUNCTIONS === */
static FredWorksScanCardWindow *
fredworks_scan_card_window_from_child( GtkWidget * widget )
{
  GtkWidget * topLevelWidget = gtk_widget_get_toplevel( widget );
  return G_OBJECT_TYPE( topLevelWidget ) == FREDWORKS_SCAN_CARD_WINDOW_TYPE ?
    FREDWORKS_SCAN_CARD_WINDOW( topLevelWidget ) :
    NULL;
}

/* === END UTILITY FUNCTIONS === */

/* === START CALLBACKS === */

static void
fredworks_scan_card_window_destroy_cb( GtkWidget * widget, void * userdata )
{
  FredWorksScanCardWindow * win = FREDWORKS_SCAN_CARD_WINDOW( widget );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );

  barcode_reader_destroy( priv->reader );
  if( priv->camera_surface )
  {
    cairo_surface_destroy( priv->camera_surface );
    priv->camera_surface = NULL;
  }
}

static gboolean
camera_view_draw_cb( GtkWidget * widget, cairo_t * cr, gpointer userdata )
{
  FredWorksScanCardWindow * win = fredworks_scan_card_window_from_child( widget );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );

  if( priv->camera_surface )
  {
    cairo_set_source_surface( cr, priv->camera_surface, 0.0, 0.0 );
    cairo_paint( cr );
    return TRUE;
  }
  else
  {
    return FALSE;
  }

}

static gboolean
camera_view_configure_event_cb( GtkWidget * widget, void * userdata )
{
  FredWorksScanCardWindow * win = fredworks_scan_card_window_from_child( widget );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );

  // Acquire a new camera buffer
  if( priv->camera_surface )
  {
    cairo_surface_destroy( priv->camera_surface );
    priv->camera_surface = NULL;
  }

  priv->camera_surface =
    cairo_image_surface_create(
      CAIRO_FORMAT_RGB24,
      gtk_widget_get_allocated_width( widget ),
      gtk_widget_get_allocated_height( widget ) );
  // No further processing necessary
  return FALSE;
}

static void
process_barcode_data( const unsigned char * barcode_data, int barcode_data_size, void * userdata )
{
  printf( "Received barcode data\n" );

  FredWorksScanCardWindow * win = fredworks_scan_card_window_from_child( GTK_WIDGET( userdata ) );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );

  FredWorksBarcodeData * data = fredworks_barcode_data_new_from_pack( barcode_data );
  if( data == NULL )
  {
    char * barcode_str = strndup( barcode_data, barcode_data_size );
    printf( "Could not load data: %s\n", barcode_str );
    free( barcode_str );
  }

  if( data != NULL )
  {
    // Process to check signature
    if( fredworks_barcode_data_check( data ) )
    {
      printf( "Data Valid\n" );
      // TODO change the validity indicator, also beep if possible
      gtk_label_set_text( priv->last_scanned_username, fredworks_barcode_data_member_name( data ) );
      gtk_label_set_text( priv->last_scanned_expiration, fredworks_barcode_data_expiration_date( data ) );
    }
    else
    {
      printf( "Data Invalid\n" );
      // TODO change the validity indicator, also beep if possible
      gtk_label_set_text( priv->last_scanned_username, fredworks_barcode_data_member_name( data ) );
      gtk_label_set_text( priv->last_scanned_expiration, fredworks_barcode_data_expiration_date( data ) );
    }
  }
}

static void
process_frame( const unsigned char * bitmap, int width, int height, void * userdata )
{
  GtkWidget * camera_view = GTK_WIDGET( userdata );
  GObject * camera_view_obj = G_OBJECT( camera_view );
  GValue requestedWidth = G_VALUE_INIT,
    requestedHeight = G_VALUE_INIT;

  g_value_init( &requestedWidth, G_TYPE_INT );
  g_value_init( &requestedHeight, G_TYPE_INT );
  g_object_get_property( camera_view_obj, "width-request", &requestedWidth );
  g_object_get_property( camera_view_obj, "height-request", &requestedHeight );

  if( g_value_get_int( &requestedWidth ) != width ||
    g_value_get_int( &requestedHeight ) != height )
  {
    g_value_set_int( &requestedWidth, width );
    g_value_set_int( &requestedHeight, height );
    // resize the object
    g_object_set_property( camera_view_obj, "width-request", &requestedWidth );
    g_object_set_property( camera_view_obj, "height-request", &requestedHeight );
    return;
  }

  FredWorksScanCardWindow * win = fredworks_scan_card_window_from_child( camera_view );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );

  if( ! priv->camera_surface )
  {
    return;
  }

  // write to a image surface
  unsigned char * camera_data = cairo_image_surface_get_data( priv->camera_surface );
  for( long i = 0; i < width * height; i++ )
  {
    *( (guint32 *) &camera_data[ i * 4 ] ) = *( (guint32 *) &bitmap[ i * 4 ] );
  }

  // trigger draw on the camera view
  gtk_widget_queue_draw( camera_view );
}

static void
camera_view_realize_cb( GtkWidget * widget, void * userdata )
{
  FredWorksScanCardWindow * win = fredworks_scan_card_window_from_child( widget );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );
  // Needs to add the callback
  barcode_reader_set_frame_callback( priv->reader, process_frame, widget);
  barcode_reader_set_barcode_callback( priv->reader, process_barcode_data, widget );
  barcode_reader_start( priv->reader );
}

static void
camera_view_unrealize_cb( GtkWidget * widget, void * userdata )
{
  FredWorksScanCardWindow * win = fredworks_scan_card_window_from_child( widget );
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );
  barcode_reader_stop( priv->reader );
}
/* === END CALLBACKS === */

static void
fredworks_scan_card_window_init( FredWorksScanCardWindow * win )
{
  gtk_widget_init_template( GTK_WIDGET( win ) );

  // Needs to open a device on Linux, gets a bit different on Windows
  // TODO it's really bad to have this hardcoded to video0
  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );
  priv->reader = barcode_reader_create( 0 );
  priv->camera_surface = NULL;
}

static void
fredworks_scan_card_window_class_init( FredWorksScanCardWindowClass * class )
{
  gtk_widget_class_set_template_from_resource(
    GTK_WIDGET_CLASS( class ),
    "/org/fwks/membership/resources/gui/FredWorksScanCardWindow.glade" );
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksScanCardWindow,
    last_scanned_username );
  gtk_widget_class_bind_template_child_private(
    GTK_WIDGET_CLASS( class ),
    FredWorksScanCardWindow,
    last_scanned_expiration );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    fredworks_scan_card_window_destroy_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    camera_view_realize_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    camera_view_unrealize_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    camera_view_configure_event_cb );
  gtk_widget_class_bind_template_callback(
    GTK_WIDGET_CLASS( class ),
    camera_view_draw_cb );
}

FredWorksScanCardWindow *
fredworks_scan_card_window_new( const char * fingerprint )
{
  FredWorksScanCardWindow * win = g_object_new(
    FREDWORKS_SCAN_CARD_WINDOW_TYPE,
    NULL );

  FredWorksScanCardWindowPrivate * priv = fredworks_scan_card_window_get_instance_private( win );
  priv->fingerprint = fingerprint;

  return win;
}

