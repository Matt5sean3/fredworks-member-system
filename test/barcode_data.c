
#include "FredWorksBarcodeData.h"
#include <string.h>
#include <gpgme.h>
#include <stdlib.h>

// The "use" idiom is useful, but awkward in C
// Might be possible to do a little less awkwardly with macros

typedef int ( * GpgmeCtxFunc )( gpgme_ctx_t ctx, void * userdata );

static int use_gpgme_ctx( GpgmeCtxFunc func, void * userdata )
{
  // Create GPGPME context
  gpgme_error_t err;
  gpgme_ctx_t ctx;
  if( ( err = gpgme_new( &ctx ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "Failed during context creation (%s)\n", gpgme_strerror( err ) );
    return 1;
  }
  int retval = func( ctx, userdata );
  gpgme_release( ctx );
  return retval;
}

typedef int ( * FileDataFunc )( gpgme_data_t file_data, void * userdata );
static int use_file_data( const char * file, FileDataFunc func, void * userdata )
{
  FILE * file_stream = fopen( file, "r" );
  if( file_stream == NULL )
  {
    printf( "Failed reading test key file\n" );
    return 1;
  }

  gpgme_error_t err;

  gpgme_data_t file_data;
  err = gpgme_data_new_from_stream( &file_data, file_stream );

  int retval = func( file_data, userdata );

  fclose( file_stream );
  return retval;
}

static int import_key_data( gpgme_data_t data, void * ctx_ptr )
{
  gpgme_ctx_t ctx = ctx_ptr;
  gpgme_error_t err = gpgme_op_import( ctx, data );
  if( err != GPG_ERR_NO_ERROR )
  {
    printf( "Failed while importing keys (%s)\n", gpgme_strerror( err ) );
    return 1;
  }
  return 0;
}

static int import_key_file_with_ctx( gpgme_ctx_t ctx, void * key_file_ptr )
{
  const char * key_file = key_file_ptr;
  return use_file_data( key_file, import_key_data, ctx );
}

static int import_key_file( const char * key_file )
{
  char * key_file_copy = strdup( key_file );
  int retval = use_gpgme_ctx( import_key_file_with_ctx, key_file_copy );
  free( key_file_copy );
  return retval;
}

static int expunge_key_data( gpgme_data_t data, void * ctx_ptr )
{
  // Remove keys found in that file from the keyring
  gpgme_ctx_t ctx = ctx_ptr;
  gpgme_error_t err;
  if( ( err = gpgme_op_keylist_from_data_start( ctx, data, 0 ) ) != GPG_ERR_NO_ERROR )
  {
    printf( "Failed while starting key listing operation while deleting (%s)", gpgme_strerror( err ) );
    return 1;
  }

  gpgme_key_t key;
  while( ( err = gpgme_op_keylist_next( ctx, &key ) ) == GPG_ERR_NO_ERROR )
  {
    gpgme_op_delete_ext( ctx, key, GPGME_DELETE_ALLOW_SECRET | GPGME_DELETE_FORCE );
  }

  if( gpgme_err_code( err ) != GPG_ERR_EOF )
  {
    printf( "Failed key listing while deleting (%s)", gpgme_strerror( err ) );
    return 1;
  }
}

static int expunge_key_file_with_ctx( gpgme_ctx_t ctx, void * key_file_ptr )
{
  const char * key_file = key_file_ptr;
  return use_file_data( key_file, expunge_key_data, ctx );
}

static int expunge_key_file( const char * key_file )
{
  char * key_file_copy = strdup( key_file );
  int retval = use_gpgme_ctx( expunge_key_file_with_ctx, key_file_copy );
  free( key_file_copy );
  return retval;
}

static int do_tests( )
{
}

int barcode_data( void )
{

  // Load up the private key for signing from file

  // Import the test key
  // 761EFD924A3D0CBEFC11DD4193FD247E794944AC

  // Initialize GPGME
  const char * version = gpgme_check_version( "1.13.1" );
  if( version == NULL )
  {
    printf( "Minimum OpenPGP Engine version not met\n" );
    return 1;
  }
  gpgme_error_t err = gpgme_engine_check_version( GPGME_PROTOCOL_OpenPGP );
  if( err != GPG_ERR_NO_ERROR ) 
  {
    printf( "OpenPGP engine not available\n" );
    return 1;
  }


  if( import_key_file( "id_ed25519" ) )
  {
    return 1;
  }

  int retval = do_tests( );

  expunge_key_file( "id_ed25519" );

  return retval;

  const char * memberName = "Matthew Balch";
  const char * expirationDate = "2019-10-20";

  FredWorksBarcodeData * data = fredworks_barcode_data_new( memberName, expirationDate );

  // 761EFD924A3D0CBEFC11DD4193FD247E794944AC
  fredworks_barcode_data_use_private_key( data, "761EFD924A3D0CBEFC11DD4193FD247E794944AC" );

  char pack[ fredworks_barcode_data_pack_size( data ) ];
  fredworks_barcode_data_to_pack( data, pack ); 

  // Read back from pack
  FredWorksBarcodeData * data2 = fredworks_barcode_data_new_from_pack( pack );
  // Check that member name can be read back
  if( strncmp( fredworks_barcode_data_member_name( data2 ), memberName, strlen( memberName ) ) != 0 )
  {
    printf( "Member name readback failed\n" );
    return 1;
  }
  // Check that expiration can be read back
  if( strncmp( fredworks_barcode_data_expiration_date( data2 ), expirationDate, strlen( expirationDate ) ) != 0 )
  {
    printf( "Expiration date readback failed\n" );
    return 1;
  }
  // Check that the data can be verified
  if( ! fredworks_barcode_data_check( data2 ) )
  {
    return 1;
  }

  fredworks_barcode_data_free( data );
  fredworks_barcode_data_free( data2 );

  return 0;
}

