
// Need to use the Reader and OpenCV directly
#include <Reader.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>

#ifdef __cplusplus
extern "C"
{
#endif

int read_barcode( );

#ifdef __cplusplus
}
#endif

int read_barcode( )
{
  // Requires a resource
  // Executes in the resources directory
  Reader reader;
  try
  {
    cv::Mat testFrame = cv::imread( "test_barcode.png", cv::IMREAD_UNCHANGED );
    std::vector< unsigned char > result =
      reader.processBarcodeFrame( testFrame );
    result.push_back( 0 );
    std::cout << "Read: " << reinterpret_cast< char * >( result.data( ) ) << std::endl;
    return 0;
  }
  catch( const std::exception & e )
  {
    std::cout << "Caught exception testing barcode reading: " << e.what( ) << std::endl;
  }
  catch( ... )
  {
    std::cout << "Caught unknown error testing barcode reading" << std::endl;
  }

  return 1;
}
