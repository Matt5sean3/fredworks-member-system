
int read_barcode( void );
int signature( void );
int barcode_data( void );
int full( void );

typedef int (*TestCb)( void );

struct TestTableEntry
{
  const char * name;
  TestCb test;
};

typedef struct TestTableEntry TestTableEntry;

extern const TestTableEntry test_table[];

