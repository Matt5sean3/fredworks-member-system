
#include "tests.h"
#include <stdio.h>
#include <string.h>

int main( int argc, char ** argv )
{
  if( argc < 2 )
  {
    printf( "No test name argument provided\n" );
    return 1;
  }
  for( int i = 0; test_table[ i ].name && test_table[ i ].test; ++i )
  {
    if( strcmp( test_table[ i ].name, argv[ 1 ] ) == 0 )
    {
      TestCb test = test_table[ i ].test;
      return test( );
    }
  }
  printf( "No test matching %s found\n", argv[ 1 ] );
  return 1;
}
