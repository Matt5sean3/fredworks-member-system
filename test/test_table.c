
#include "tests.h"

int hash_user( void );

const TestTableEntry test_table[] = {
  { "read_barcode", read_barcode },
  { "barcode_data", barcode_data },
  { "full", full },
  { 0, 0 } // null-termination
};
 
