
#include "Reader.hpp"

#include <exception>
#include <algorithm>

#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
// Reed-Solomon error correction
#include <rs_decode.h>

// Reconstructs the Aztec message from the sequence of aztec symbols
#include "decode_aztec.hpp"

static bool
checkImageBounds( cv::Mat mat, std::vector< cv::Point2d > points );

Reader_::Reader_( int device ) :
  captureIdentifier( device ),
  frameCb( 0 ), // null pointer
  barcodeCb( 0 ), // null pointer
  halt( false )
{
}

Reader_::Reader_( ) :
  captureIdentifier( 0 ),
  frameCb( 0 ), // null pointer
  barcodeCb( 0 ), // null pointer
  halt( false )
{
}

Reader_::~Reader_( )
{
  stop( );
}

void
Reader_::start( )
{
  halt = false;
  if( ! cap.isOpened( ) )
  {
    cap.open( captureIdentifier + cv::CAP_ANY );
  }
  readThread = std::thread( threadMethod, this );
}

void
Reader_::stop( )
{
  halt = true;
  if( readThread.joinable( ) )
  {
    readThread.join( );
  }
}

void
Reader_::cycleCaptureSource( )
{
  if( readThread.joinable( ) )
  {
    stop( );
  }

  while( ! cap.isOpened( ) )
  {
    cap.open( captureIdentifier + cv::CAP_ANY );
    if( ! cap.isOpened( ) && captureIdentifier == 0 )
    {
      // Can't start, no camera available
      return;
    }
    ++captureIdentifier;
  }

  start( );
}

void
Reader_::setFrameCallback( FrameCb cb, void * userdata )
{
  frameCb = cb;
  frameCbUserdata = userdata;
}

void
Reader_::setBarcodeCallback( BarcodeCb cb, void * userdata )
{
  barcodeCb = cb;
  barcodeCbUserdata = userdata;
}

static bool
checkImageBounds( cv::Mat mat, std::vector< cv::Point2d > points )
{
  for( auto iter = points.begin( ); iter != points.end( ); ++iter )
  {
    if( iter->y >= mat.rows || iter->y < 0 || iter->x  >= mat.cols || iter->x < 0 )
    {
      return false;
    }
  }
  return true;
}

static int
traverse_direct_hierarchy(
  const std::vector< cv::Vec4i > hierarchy,
  int idx,
  int & depth )
{
  cv::Vec4i node = hierarchy[ idx ];
  if( node[ 0 ] < 0 &&
      node[ 1 ] < 0 &&
      node[ 2 ] >= 0 &&
      node[ 3 ] >= 0 )
  {
    // check the parent
    depth += 1;
    return traverse_direct_hierarchy( hierarchy, node[ 3 ], depth );
  }
  else
  {
    return idx;
  }
}

static uint8_t
computeImageMean( cv::Mat img )
{
  uint32_t total = 0;

  for( int i = 0; i < img.rows; i++ )
  {
    for( int j = 0; j < img.cols; j++ )
    {
      uint8_t shade = img.at< uint8_t >( i, j );
      total += shade;
    }
  }
  return ( uint8_t ) ( total / ( img.rows * img.cols ) );
}

// Returns true and stores the simplified contours of the target in the contours vector if a target is found
// Returns false and doesn't modify the contours vector if no target can be found
//
// "img" should be a binary image
// "contours" should be empty
static bool
findAztecTargetContours( const cv::Mat img, std::vector< std::vector< cv::Point > > & targetContours )
{
  // findContours to find the target
  std::vector< std::vector< cv::Point > > contours;
  std::vector< cv::Vec4i > contourHierarchy;

  // Compute the full hierarchy for contours
  cv::findContours(
    img,
    contours,
    contourHierarchy,
    cv::RETR_TREE,
    cv::CHAIN_APPROX_SIMPLE );

  // Find the deepest direct hierarchy to find the target
  int deepestRoot = -1;
  int deepestTrunk = -1;
  int deepestDepth = 0;
  for(
    int i = 0;
    i < contourHierarchy.size( );
    ++i )
  {
    int depth = 0;
    int trunk = traverse_direct_hierarchy( contourHierarchy, i, depth );
    if( depth > deepestDepth )
    {
      deepestDepth = depth;
      deepestTrunk = trunk;
      deepestRoot = i;
    }
  }

  // Couldn't find a target
  if( deepestRoot == -1 || deepestDepth < 4 )
  { 
    return false;
  }

  bool targetStable = deepestDepth >= 4;
  std::vector< std::vector< cv::Point > > targetContourApproximations;
  int idx = deepestRoot;

  while( idx != deepestTrunk )
  {
    std::vector< cv::Point > contourApproximation;
    cv::approxPolyDP( contours[ idx ], contourApproximation, 4.0, true );
    targetContours.push_back( contourApproximation );
    if( contourApproximation.size( ) != 4 )
    { // approximated contours must have only 4 vertices
      return false;
    }

    idx = contourHierarchy[ idx ][ 3 ];
  }
  {
    std::vector< cv::Point > contourApproximation;
    cv::approxPolyDP( contours[ idx ], contourApproximation, 4.0, true );
    targetContours.push_back( contourApproximation );
    if( contourApproximation.size( ) != 4 )
    { // approximated contours must have only 4 vertices
      return false;
    }
  }
  return true;
}

// Complex version that can actually work semi-efficiently
static std::vector< cv::Point2d >
rotateDataLocations(
  const std::vector< cv::Point2d > & first_locations,
  std::vector< cv::Point2d > & result,
  int length,
  bool cw = true,
  int sourceStart = 0,
  int resultStart = 0 )
{
  if( result.size( ) < resultStart + 4 * length )
  {
    result.resize( resultStart + 4 * length );
  }

  // Copy the initial array
  if( &first_locations != &result )
  {
    for( int i = 0; i < length; ++i )
    {
      result[ i + resultStart ] = first_locations[ i + sourceStart ];
    }
  }

  for(
    int i = length;
    i < 4 * length;
    ++i )
  {
    cv::Point2d & previousSide = result[ i - length + resultStart ];
    result[ i + resultStart ] = cw ? cv::Point2d( -previousSide.y, previousSide.x ) : cv::Point2d( previousSide.y, -previousSide.x );
  }

  return result;
}

// Simple version
static std::vector< cv::Point2d >
rotateDataLocations( const std::vector< cv::Point2d > & first_locations, bool cw = true )
{
  int length = first_locations.size( );
  std::vector< cv::Point2d > result = std::vector< cv::Point2d >( length * 4 );
  return rotateDataLocations( first_locations, result, length, cw );
}

static std::vector< cv::Point2d >
orientationMarkLocations_( )
{
  static const double baseOrientationMarkLocations[ 6 ] = {
    -7.0, -6.0,
    -7.0, -7.0,
    -6.0, -7.0
  };
  std::vector< cv::Point2d > orientationMarkLocations( 3 );
  for( int i = 0; i < 3; ++i )
  {
    orientationMarkLocations[ i ] =
      cv::Point2d(
        baseOrientationMarkLocations[ i * 2 ],
        baseOrientationMarkLocations[ i * 2 + 1 ] );
  }
  return rotateDataLocations( orientationMarkLocations );
}

static const std::vector< cv::Point2d >
orientationMarkLocations( )
{
  // primitive memoization
  static const std::vector< cv::Point2d > locations = orientationMarkLocations_( );
  return locations;
}

static std::vector< cv::Point2d >
adjustForAztecReferenceGrid( const std::vector< cv::Point2d > & points )
{
  std::vector< cv::Point2d > result = points;
  for( std::vector< cv::Point2d >::iterator pointIter = result.begin( ); pointIter != result.end( ); ++pointIter )
  {
    cv::Point2d & point = *pointIter;
    point.x -= point.x <= 0;
    point.y -= point.y <= 0;
    point.x += static_cast< int >( point.x ) / 16;
    point.y += static_cast< int >( point.y ) / 16;
  }
  return result;
}

static std::vector< cv::Point2d >
modeDataLocations_( )
{
  std::vector< cv::Point2d > modeDataLocations( 10 );
  for( int i = 0; i < 10; ++i )
  {
    modeDataLocations[ i ] = cv::Point2d( -4.0 + i, -6.0 );
  }
  return rotateDataLocations( adjustForAztecReferenceGrid( modeDataLocations ) );
}

static const std::vector< cv::Point2d >
modeDataLocations( )
{
  // Some primitive memoization
  static const std::vector< cv::Point2d > dataLocations = modeDataLocations_( );
  return dataLocations;
}

static int codewordSizeFromLayers( unsigned int numLayers )
{
  return numLayers == 0 ? 4 :
    numLayers <= 2 ? 6 :
    numLayers <= 8 ? 8 :
    numLayers <= 22 ? 10 :
    numLayers <= 32 ? 12 :
    -1;
}

static int gfpolyFromLayers( unsigned int numLayers )
{
  return
    numLayers == 0 ?  0b0000000000010011 : // x^4 + x + 1
    numLayers <= 2 ?  0b0000000001000011 : // x^6 + x + 1
    numLayers <= 8 ?  0b0000000100101101 : // x^8 + x^5 + x^3 + x^2 + 1
    numLayers <= 22 ? 0b0000010000001001 : // x^10 + x^3 + 1
    numLayers <= 32 ? 0b0001000001101001 : // x^12 + x^6 + x^5 + x^3 + 1
    -1;
}

static std::vector< cv::Point2d >
gridLocations( cv::Point2d start, cv::Size2d stride, cv::Size2i size )
{
  std::vector< cv::Point2d > grid( size.width * size.height );
  cv::Point2d cursor( start );
  for( int i = 0; i < size.height; ++i )
  {
    cursor.x = start.x;
    for( int j = 0; j < size.width; ++j )
    {
      grid[ i * size.width + j ] = cursor;
      cursor.x += stride.width;
    }
    cursor.y += stride.height;
  }
  return grid;
}

static std::vector< cv::Point2d >
subLayerDataLocations( int subLayer, bool adjust )
{
  int layerSideLength = 2 * subLayer + 16 + !adjust;
  double layerCorner = -8.0 - subLayer;
  std::vector< cv::Point2d > singleSide( gridLocations( cv::Point2d( layerCorner, layerCorner ), cv::Size2d( 1.0, 1.0 ), cv::Size2i( 2, layerSideLength ) ) );
  return adjust ? rotateDataLocations( adjustForAztecReferenceGrid( singleSide ), false ) : rotateDataLocations( singleSide, false );
}

static std::vector< cv::Point2d >
singleLayerDataLocations( int layer )
{
  return subLayerDataLocations( 2 * layer, true );
}

static std::vector< cv::Point2d >
layerDataLocations( int layer, std::vector< cv::Point2d > & result, int skip = 0, int start = 0 )
{
  std::vector< cv::Point2d > layerLocations = singleLayerDataLocations( layer );
  for( int i = skip; i < layerLocations.size( ); ++i )
  {
    result[ i - skip + start ] = layerLocations[ i ];
  }

  if( layer >= 1 )
  {
    layerDataLocations( layer - 1, result, 0, start + layerLocations.size( ) - skip );
  }

  return result;
}

static std::vector< cv::Point2d >
layerDataLocations( int layers )
{
  // Get the codeword size to determine the number of bits skipped
  int codewordSize = codewordSizeFromLayers( layers );

  int dataSquareSideLength = ( 4 * layers + 14 );
  int dataArea = dataSquareSideLength * dataSquareSideLength - 14 * 14;
  // ensure the number of bits is a multiple of the codeword size
  std::vector< cv::Point2d > result( dataArea - ( dataArea % codewordSize ) );

  layerDataLocations( layers - 1, result, dataArea % codewordSize );
  return result;
}

// Needs support for both unsigned char and unsigned int codewords
template< class T >
static std::vector< T >
readLocations(
  cv::Mat source,
  const std::vector< cv::Point2d > & locations,
  int codeword_size,
  bool msbFirst = true )
{
  std::vector< T > values;
  for( unsigned int i = 0; i < locations.size( ); ++i )
  {
    unsigned char place = msbFirst ? ( codeword_size - 1 ) - ( i % codeword_size ) : i % codeword_size;
    if( i % codeword_size == 0 )
    {
      // start a new codeword
      values.push_back( 0 );
    }
    T & codeword = values.back( );

    bool value = source.at< uint8_t >( locations[ i ] ) == 0;
    codeword |= value ? ( 1 << place ) : 0;
  }
  return values;
}

template < class T, class S >
std::vector< S > contourConversion( std::vector< T > contour )
{
  std::vector< S > newContour( contour.size( ) );
  for( int i = 0; i < contour.size( ); ++i )
  {
    newContour[ i ] = S( contour[ i ] );
  }
  return newContour;
}

template < class T >
std::vector< int >
locateErasures( std::vector< T > codewords, int codewordSize )
{
  std::vector< int > erasures;
  T allOnes = ( 1 << codewordSize ) - 1;
  for( int i = 0; i < codewords.size( ); ++i )
  {
    codewords[ i ] == 0 || codewords[ i ] == allOnes;
  }
  return erasures;
}

// Sort the points by distance from the given point
class DistanceSortFunctor
{
private:
  cv::Point2d center;
public:
  DistanceSortFunctor( cv::Point2d aCenter ) :
    center( aCenter )
  {
  }
  bool operator() ( cv::Point2d left, cv::Point2d right )
  {
    double
      leftDx = left.x - center.x,
      leftDy = left.y - center.y,
      rightDx = right.x - center.x,
      rightDy = right.y - center.y;
    return leftDx * leftDx + leftDy * leftDy > rightDx * rightDx + rightDy * rightDy;
  }
};

std::vector< unsigned char >
Reader::processBarcodeFrame( cv::Mat & frame )
{
  if( frame.empty( ) )
  {
    // Something might be up, but don't crash
    // Frame has type of 16 initially
    throw std::runtime_error( "Received empty frame." );
  }

  // Downconvert to gray scale
  cv::Mat grayFrame;
  cv::cvtColor( frame, grayFrame, cv::COLOR_RGB2GRAY );

  // Search for corners in the gray frame
  std::vector< cv::KeyPoint > corners;
  cv::FAST( grayFrame, corners, 50, false );
  std::vector< cv::Point2d > cornerPoints( corners.size( ) );
  for( int i = 0; i < corners.size( ); ++i )
  {
    cv::Point2f point = corners[ i ].pt;
    cornerPoints[ i ] = cv::Point2d( point.x, point.y );
  }

  // thresholding with pixel bleed may be the problem
  cv::Mat bwFrame;
  cv::threshold( grayFrame, bwFrame, (double) computeImageMean( grayFrame ), 255, cv::THRESH_BINARY );

  // findContours to find the target
  std::vector< std::vector< cv::Point > > targetContours;
  if( ! findAztecTargetContours( bwFrame, targetContours ) )
  {
    throw std::runtime_error( "Unable to locate aztec barcode" );
  }
  cv::drawContours( frame, targetContours, -1, cv::Scalar( 255.0, 0.0, 0.0, 0.0 ), 2 );

  // Find corner key points along the 4th contour
  cv::Mat transform;

  // Finding nested contours isn't that bad of a method to find the target
  std::vector< cv::Point2d > pixelLocations;
  pixelLocations.push_back( cv::Point2d(  5.5, -5.5 ) ); // top-right
  pixelLocations.push_back( cv::Point2d( -5.5, -5.5 ) ); // top-left
  pixelLocations.push_back( cv::Point2d( -5.5,  5.5 ) ); // bottom-left
  pixelLocations.push_back( cv::Point2d(  5.5,  5.5 ) ); // bottom-right

  transform = cv::findHomography( pixelLocations, targetContours[ 4 ] );

  // Find the center
  std::vector< cv::Point2d > centerPoint;
  centerPoint.push_back( cv::Point2d( 0.0, 0.0 ) );
  std::vector< cv::Point2d > imageCenterPoint;
  cv::perspectiveTransform( centerPoint, imageCenterPoint, transform );

  // Re-order based on distance from the barcode center
  std::sort( cornerPoints.begin( ), cornerPoints.end( ), DistanceSortFunctor( imageCenterPoint[ 0 ] ) );

  // Iteratively recompute the transform, 20% at a time, ignore last 20%
  for( int i = 0; i < 4; ++i )
  {
    std::vector< cv::Point2d > partialCornerPoints( cornerPoints.begin( ), cornerPoints.begin( ) + static_cast< int >( ::floor( ( cornerPoints.size( ) - 1 ) * static_cast< double >( i + 1 ) / 5.0 ) ) );
    std::vector< cv::Point2d > pixelCornerPoints;
    cv::perspectiveTransform( partialCornerPoints, pixelCornerPoints, transform.inv( ) );
    // round corners to the nearest expected grid location
    for( std::vector< cv::Point2d >::iterator pointIter = pixelCornerPoints.begin( );
        pointIter != pixelCornerPoints.end( );
        ++pointIter )
    {
      *pointIter = cv::Point2d( ::round( pointIter->x - 0.5 ) + 0.5, ::round( pointIter->y - 0.5 ) + 0.5 );
    }

    transform = cv::findHomography( pixelCornerPoints, partialCornerPoints );
  }

  // The transform should resolve orientation, the contours should be otherwise aligned

  std::vector< cv::Point2d > orientationMarkImageLocations;
  cv::perspectiveTransform( orientationMarkLocations( ), orientationMarkImageLocations, transform );

  if( ! checkImageBounds( bwFrame, orientationMarkImageLocations ) )
  {
    throw std::runtime_error( "Attempt to read orientation marks beyond image bounds." );
  }
  std::vector< uint8_t > orientationCodes =
    readLocations< uint8_t >( bwFrame, orientationMarkImageLocations, 3 );

  int start;
  for( start = 0; start < 4 && orientationCodes[ start ] != 7; ++start );
  if( start == 4 )
  {
    throw std::runtime_error( "Failed to find initial orientation corner" );
  }

  // verify that the orientation sequence is good
  static const uint8_t expectedOrientationSequence[] = { 7, 3, 4, 0 };
  for( int i = 0; i < 4; ++i )
  {
    if( orientationCodes[ ( i + start ) % 4 ] != expectedOrientationSequence[ i ] )
    {
      throw std::runtime_error( "Unable to orient" );
    }
  }

  // prepend orientation compensation to the transform
  static const int coeffs[] =
    { 1,  0,
      0,  1,

      0, -1,
      1,  0,

      -1, 0,
      0, -1,

      0,  1,
      -1,  0 };

  const int * usedCoeffs = coeffs + start * 4;

  cv::Mat orientationMatrix( 3, 3, CV_64F );
  orientationMatrix.at< double >( 0, 0 ) = usedCoeffs[ 0 ]; orientationMatrix.at< double >( 0, 1 ) = usedCoeffs[ 1 ]; orientationMatrix.at< double >( 0, 2 ) = 0;
  orientationMatrix.at< double >( 1, 0 ) = usedCoeffs[ 2 ]; orientationMatrix.at< double >( 1, 1 ) = usedCoeffs[ 3 ]; orientationMatrix.at< double >( 1, 2 ) = 0;
  orientationMatrix.at< double >( 2, 0 ) = 0;               orientationMatrix.at< double >( 2, 1 ) = 0;               orientationMatrix.at< double >( 2, 2 ) = 1;

  transform = transform * orientationMatrix;

  // 16-bits of actual data as 4, 4-bit codewords and 24-bits of RS check words, 6 total check words
  std::vector< cv::Point2d > modeDataImageLocations;
  cv::perspectiveTransform( modeDataLocations( ), modeDataImageLocations, transform );

  if( ! checkImageBounds( bwFrame, modeDataImageLocations ) )
  {
    throw std::runtime_error( "Attempt to read mode data beyond image bounds." );
  }
  std::vector< uint8_t > rsWords =
    readLocations< uint8_t >( bwFrame, modeDataImageLocations, codewordSizeFromLayers( 0 ) );

  // N = 2^symsize - pad - 1 bits, should be 15 code words for 4 bits
  {
    // Create an RS codec for the inner data
    // 10 symbols, need 15 to fit, so 5 pad symbols
    // 4 data symbols, 6 check symbols
    // No knowledge of erasures
    void* mode_rs_codec = init_rs_char( codewordSizeFromLayers( 0 ), gfpolyFromLayers( 0 ), 1, 1, 6, 5 );
    int corrections = decode_rs_char( mode_rs_codec, rsWords.data( ), NULL, 0 );
    free_rs_char( mode_rs_codec );
    if( corrections < 0 )
    {
      throw std::runtime_error( "Could not decode mode data" );
    }
  }

  // 5 layer bits
  int numLayers = ( ( rsWords[ 0 ] << 1 ) + ( ( rsWords[ 1 ] & 0x08 ) >> 3 ) ) + 1;
  // 11 bits for data codeword count, encoded as number_of_codewords - 1
  int numDataCodewords = ( ( rsWords[ 1 ] & 0x07 ) << 8 ) + ( rsWords[ 2 ] << 4 ) + rsWords[ 3 ] + 1;

  // With knowledge of the number of layers, it's possible to start looking for reference features to make a more stable transform
  // NOTE: it's important that this be integer arigthmetic exactly as currently written, as this will break if the remainder is not discarded
  int codewordSize = codewordSizeFromLayers( numLayers );
  std::vector< cv::Point2d > ldl = layerDataLocations( numLayers );

  std::vector< cv::Point2d > ldlImage;
  cv::perspectiveTransform( ldl, ldlImage, transform );

  if( ! checkImageBounds( bwFrame, ldlImage ) )
  {
    throw std::runtime_error( "Attempt to read barcode data beyond image bounds." );
  }

  std::vector< std::vector< cv::Point > > ldlContours;
  ldlContours.push_back( contourConversion< cv::Point2d, cv::Point >( ldlImage ) );

  cv::drawContours( frame, ldlContours, -1, cv::Scalar( 0.0, 0.0, 255.0, 0.0 ), 2 );

  // There's a heck of a lot of pad necessary
  if( codewordSize > 8 )
  {
    std::vector< unsigned int > rsWords = readLocations< unsigned int >( bwFrame, ldlImage, codewordSize );
    // n = 2^b - 1 codewords
    // padding = n - numWords
    void * rs_codec = init_rs_int(
      codewordSize,
      gfpolyFromLayers( numLayers ),
      1, 1,
      rsWords.size( ) - numDataCodewords,
      ( 1 << codewordSize ) - 1 - rsWords.size( ) );
    std::vector< int > erasureIdxs = locateErasures( rsWords, codewordSize );
    int corrections = decode_rs_int( rs_codec, rsWords.data( ), erasureIdxs.data( ), erasureIdxs.size( ) );
    free_rs_int( rs_codec );
    if( corrections < 0 )
    {
      throw std::runtime_error( "Could not decode content" );
    }
    rsWords.resize( numDataCodewords );
    return decodeAztecBits( rsWords, codewordSize );
  }
  else
  {
    std::vector< unsigned char > rsWords =
      readLocations< unsigned char >( bwFrame, ldlImage, codewordSize );
    // Erasures occurred wherever all ones or all zeros are
    // 10 symbols, need 15 to fit, so 5 pad symbols
    // expects 2^b - 1 - padding codewords, n = 2^4 - 1 - padding, k = 
    int corrections = 0;
    void* rs_codec = init_rs_char(
      codewordSize,
      gfpolyFromLayers( numLayers ),
      1, 1,
      rsWords.size( ) - numDataCodewords,
      ( 1 << codewordSize ) - 1 - rsWords.size( ) ); // amount of padding to reach 2^b - 1 codewords
    std::vector< int > erasureIdxs = locateErasures( rsWords, codewordSize );
    corrections = decode_rs_char( rs_codec, rsWords.data( ), erasureIdxs.data( ), erasureIdxs.size( ) );
    free_rs_char( rs_codec );
    if( corrections < 0 )
    {
      throw std::runtime_error( "Could not decode content" );
    }
    rsWords.resize( numDataCodewords );
    return decodeAztecBits( rsWords, codewordSize );
  }
}

void
Reader_::threadMethod( Reader * pReader )
{
  if( ! pReader ) // Abort if given a null pointer
  {
    return;
  }

  // Change the pointer to a reference
  Reader & reader = * pReader;
  cv::Mat frame;

  while( ! reader.halt )
  {
    // Read a matrix frame
    std::vector< unsigned char > data; 
    reader.cap.read( frame );
    try
    {
      std::vector< unsigned char > barcodeData( reader.processBarcodeFrame( frame ) );
      if( reader.barcodeCb )
      {
        reader.barcodeCb( barcodeData.data( ), barcodeData.size( ), reader.barcodeCbUserdata );
      }
    }
    catch( std::exception & )
    {
    }

    if( reader.frameCb )
    {
      // Type is 16, 1 << 4
      // convert the frame into the RGB24 format
      // Converting into some grayscale format may be faster, so maybe try that later
      unsigned char camera_rgb24_data[ 4 * frame.cols * frame.rows ];
      for( int i = 0; i < frame.rows; i++ )
      {
        for( int j = 0; j < frame.cols; j++ )
        {
          // average frame and gray frame
          uint32_t color = *( ( uint32_t * ) frame.ptr( i, j ) );

          uint32_t
            blue = color & 0x0000FF,
            green = ( color & 0x00FF00 ) >> 8,
            red = ( color & 0xFF0000 ) >> 16;

          *( ( uint32_t * ) (&camera_rgb24_data[ 4 * ( i * frame.cols + j ) ] ) ) = blue + ( green << 8 ) + ( red << 16 );
        }
      }
      reader.frameCb( camera_rgb24_data, frame.cols, frame.rows, reader.frameCbUserdata );
    }

  }
}
