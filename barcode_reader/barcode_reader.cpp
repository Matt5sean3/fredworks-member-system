
#include <memory>

#include "barcode_reader.h"
#include "Reader.hpp"

Reader *
barcode_reader_create( int device )
{
  // Absolutely cannot leak any exceptions into C
  try
  {
    return new Reader_( device );
  }
  catch( ... )
  {
    printf( "Error during Reader construction\n" );
    return NULL;
  }
}

void
barcode_reader_destroy( Reader * reader )
{
  if( ! reader )
  {
    return;
  }
  delete reader;
}

void
barcode_reader_start( Reader * reader )
{
  if( ! reader )
  {
    return;
  }
  // Starts a separate thread to actually capture video
  reader->start( );
}

void
barcode_reader_stop( Reader * reader )
{
  if( ! reader )
  {
    return;
  }
  // Causes the separate thread to stop capturing video and join
  reader->stop( );
}

void
barcode_reader_cycle_source( Reader * reader )
{
  reader->cycleCaptureSource( );
}

void
barcode_reader_set_frame_callback( Reader * reader, FrameCb frame_cb, void * userdata )
{
  if( ! reader )
  {
    return;
  }
  reader->setFrameCallback( frame_cb, userdata );
}

void
barcode_reader_set_barcode_callback( Reader * reader, BarcodeCb barcode_cb, void * userdata )
{
  if( ! reader )
  {
    return;
  }
  reader->setBarcodeCallback( barcode_cb, userdata );
}
