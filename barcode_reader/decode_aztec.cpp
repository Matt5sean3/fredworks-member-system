
#include "decode_aztec.hpp"
#include <stdexcept>

int 
decodeAztecUpperBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved )
{
  if( word == 0 )
  {
    state.shiftMode = AZTEC_PUNCT;
  }
  else if( word == 1 )
  {
    retrieved[ 0 ] = ' ';
    return 1;
  }
  else if( word >= 2 && word <= 27 )
  {
    retrieved[ 0 ] = 'A' + word - 2;
    return 1;
  }
  else if( word == 28 )
  {
    state.currentMode = AZTEC_LOWER;
  }
  else if( word == 29 )
  {
    state.currentMode = AZTEC_MIXED;
  }
  else if( word == 30 )
  {
    state.currentMode = AZTEC_DIGIT;
  }
  else if( word == 31 )
  {
    state.shiftMode = AZTEC_BINARY;
  }
  return 0;
}

int
decodeAztecLowerBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved )
{
  if( word == 0 )
  {
    state.shiftMode = AZTEC_PUNCT;
  }
  else if( word == 1 )
  {
    retrieved[ 0 ] = ' ';
    return 1;
  }
  else if( word >= 2 && word <= 27 )
  {
    retrieved[ 0 ] = 'a' + word - 2;
    return 1;
  }
  else if( word == 28 )
  {
    state.shiftMode = AZTEC_UPPER;
  }
  else if( word == 29 )
  {
    state.currentMode = AZTEC_MIXED;
  }
  else if( word == 30 )
  {
    state.currentMode = AZTEC_DIGIT;
  }
  else if( word == 31 )
  {
    state.shiftMode = AZTEC_BINARY;
  }
  return 0;
}

int
decodeAztecMixedBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved )
{
  if( word == 0 )
  {
    state.shiftMode = AZTEC_PUNCT;
  }
  else if( word == 1 )
  {
    retrieved[ 0 ] = ' ';
    return 1;
  }
  else if( word >= 2 && word <= 14 )
  {
    retrieved[ 0 ] = word - 1;
    return 1;
  }
  else if( word >= 15 && word <= 19 )
  {
    retrieved[ 0 ] = word + 12;
    return 1;
  }
  else if( word == 20 )
  {
    retrieved[ 0 ] = '@';
    return 1;
  }
  else if( word == 21 )
  {
    retrieved[ 0 ] = '\\';
    return 1;
  }
  else if( word >= 22 && word <= 24 )
  {
    retrieved[ 0 ] = '^' + word - 22;
    return 1;
  }
  else if( word == 25 )
  {
    retrieved[ 0 ] = '|';
    return 1;
  }
  else if( word == 26 )
  {
    retrieved[ 0 ] = '~';
    return 1;
  }
  else if( word == 27 )
  {
    // ^? is delete?
    retrieved[ 0 ] = 127;
    return 1;
  }
  else if( word == 28 )
  {
    state.currentMode = AZTEC_LOWER;
  }
  else if( word == 29 )
  {
    state.currentMode = AZTEC_UPPER;
  }
  else if( word == 30 )
  {
    state.currentMode = AZTEC_PUNCT;
  }
  else if( word == 31 )
  {
    state.shiftMode = AZTEC_BINARY;
  }
  return 0;
}

int
decodeAztecPunctBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved )
{
  if( word == 0 )
  { // FLG(n), hmm I'm not really equipped to handle this
    // unsigned char n = readAztecBits( codewords, currentCodeword, currentBit, 3, codewordSize );
    // Process 4 digits
    // for( int i = 0; i < n; ++i )
    // {
    //   readAztecBits( codewords, currentCodeword, currentBit, 4, codewordSize );
    // }
  }
  else if( word == 1 )
  {
    retrieved[ 0 ] = '\r';
    return 1;
  }
  else if( word == 2 )
  {
    // I forgot about the double characters
    retrieved[ 0 ] = '\r';
    retrieved[ 1 ] = '\n';
    return 2;
  }
  else if( word == 3 )
  {
    retrieved[ 0 ] = '.';
    retrieved[ 1 ] = ' ';
    return 2;
  }
  else if( word == 4 )
  {
    retrieved[ 0 ] = ',';
    retrieved[ 1 ] = ' ';
    return 2;
  }
  else if( word == 5 )
  {
    retrieved[ 0 ] = ':';
    retrieved[ 1 ] = ' ';
    return 2;
  }
  else if( word >= 6 && word <= 20 )
  {
    retrieved[ 0 ] = '!' - 6 + word;
    return 1;
  }
  else if( word >= 21 && word <= 26 )
  {
    retrieved[ 0 ] = ':' - 21 + word;
    return 1;
  }
  else if( word == 27 )
  {
    retrieved[ 0 ] = '[';
    return 1;
  }
  else if( word == 28 )
  {
    retrieved[ 0 ] = ']';
    return 1;
  }
  else if( word == 29 )
  {
    retrieved[ 0 ] = '{';
    return 1;
  }
  else if( word == 30 )
  {
    retrieved[ 0 ] = '}';
    return 1;
  }
  else if( word == 31 )
  {
    state.currentMode = AZTEC_UPPER;
  }
  return 0;
}

int
decodeAztecDigitBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved )
{
  if( word == 0 )
  {
    state.shiftMode = AZTEC_PUNCT;
  }
  else if( word == 1 )
  {
    retrieved[ 0 ] = ' ';
    return 1;
  }
  else if( word >= 2 && word <= 11 )
  {
    retrieved[ 0 ] = '0' + word - 2;
    return 1;
  }
  else if( word == 12 )
  {
    retrieved[ 0 ] = ',';
    return 1;
  }
  else if( word == 13 )
  {
    retrieved[ 0 ] = '.';
    return 1;
  }
  else if( word == 14 )
  {
    state.currentMode = AZTEC_UPPER;
  }
  else if( word == 15 )
  {
    state.shiftMode = AZTEC_UPPER;
  }
  return 0;
}

