
#ifndef BARCODE_READER_H_
#define BARCODE_READER_H_
// Linkage to C

#ifdef __cplusplus
extern "C" {
#endif

#include "Reader_fwd.h"

// The bitmap is provided in a configurable format
// Processed in a separate thread, so be careful of thread-safety on this

Reader *
barcode_reader_create( int device );

void
barcode_reader_destroy( Reader * reader );

void
barcode_reader_start( Reader * reader );

void
barcode_reader_stop( Reader * reader );

void
barcode_reader_cycle_source( Reader * reader );

void
barcode_reader_set_frame_callback( Reader * reader, FrameCb frame_cb, void * userdata );

void
barcode_reader_set_barcode_callback( Reader * reader, BarcodeCb barcode_cb, void * userdata );

#ifdef __cplusplus
}
#endif

#endif // BARCODE_READER_H_

