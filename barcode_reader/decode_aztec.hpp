
#ifndef DECODE_AZTEC_H_
#define DECODE_AZTEC_H_

#include <vector>
#include <stdexcept>

// This is meant to be an internal header. Due to the need for templates the encapsulation here is not as good as I'd like

enum AztecStringState
{
  AZTEC_UPPER,
  AZTEC_LOWER,
  AZTEC_MIXED,
  AZTEC_PUNCT,
  AZTEC_DIGIT,
  AZTEC_BINARY,
  AZTEC_NONE
};

struct AztecDecoderState
{
  int currentCodeword;
  int currentBit;
  int codewordSize;
  AztecStringState currentMode;
  AztecStringState shiftMode;
};

// forward defines
int decodeAztecUpperBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved );

int decodeAztecLowerBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved );

int decodeAztecMixedBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved );

int decodeAztecPunctBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved );

int decodeAztecDigitBits(
  unsigned char word,
  AztecDecoderState & state,
  unsigned char * retrieved );

template < class T >
unsigned char
readAztecBits(
    const std::vector< T > & codewords,
    AztecDecoderState & state,
    int nBits )
{
  if( codewords.size( ) <= state.currentCodeword )
  {
    return 0;
  }
  unsigned char word;
  static const T stuffedZeroes = 1;
  static const T stuffedOnes = ( 1 << state.codewordSize ) - 2;
  // Omit stuffing bits
  int adjustedCodewordSize =
    codewords[ state.currentCodeword ] == stuffedZeroes ||
    codewords[ state.currentCodeword ] == stuffedOnes ?
      state.codewordSize - 1 :
      state.codewordSize;
  if( state.currentBit + nBits > adjustedCodewordSize )
  {
    // read the rest of this codeword and get the rest from the next one
    int partialRead = adjustedCodewordSize - state.currentBit;
    // Copy the current state
    AztecDecoderState oldState = state;
    state.currentCodeword += 1;
    state.currentBit = 0;
    return ( readAztecBits( codewords, oldState, partialRead ) << ( nBits - partialRead ) ) +
      readAztecBits( codewords, state, nBits - partialRead );
  }
  else
  {
    T readBits = ( ( 1 << state.codewordSize ) - 1 ) >> state.currentBit;
    readBits -= readBits >> nBits;
    state.currentBit += nBits;
    return ( codewords[ state.currentCodeword ] & readBits ) >> ( state.codewordSize - state.currentBit );
  }
}

template < class T >
std::vector< unsigned char >
decodeAztecBits( std::vector< T > codewords, int codewordSize )
{
  std::vector< unsigned char > message;
  AztecDecoderState state;
  state.currentBit = 0;
  state.currentCodeword = 0;
  state.codewordSize = codewordSize;
  state.currentMode = AZTEC_UPPER;
  state.shiftMode = AZTEC_NONE;

  while( state.currentCodeword < codewords.size( ) )
  {
    AztecStringState immediateMode = state.shiftMode == AZTEC_NONE ? state.currentMode : state.shiftMode;
    state.shiftMode = AZTEC_NONE;

    int nReceived;
    unsigned char characters[ 2 ];
    switch( immediateMode )
    {
    case AZTEC_UPPER:
      nReceived = decodeAztecUpperBits( readAztecBits( codewords, state, 5 ), state, characters );
      break;
    case AZTEC_LOWER:
      nReceived = decodeAztecLowerBits( readAztecBits( codewords, state, 5 ), state, characters );
      break;
    case AZTEC_MIXED:
      nReceived = decodeAztecMixedBits( readAztecBits( codewords, state, 5 ), state, characters );
      break;
    case AZTEC_PUNCT:
      nReceived = decodeAztecPunctBits( readAztecBits( codewords, state, 5 ), state, characters );
      break;
    case AZTEC_DIGIT:
      nReceived = decodeAztecDigitBits( readAztecBits( codewords, state, 4 ), state, characters );
      break;
    case AZTEC_BINARY:
    {
      // binary is a special case
      unsigned int length = readAztecBits( codewords, state, 5 );
      length = length == 0 ? readAztecBits( codewords, state, 11 ) : length;
      for( int i = 0; i < length; ++i )
      {
        // Read the data straight
        message.push_back( readAztecBits( codewords, state, 8 ) );
      }
      nReceived = 0;
    }
      break;
    default:
      throw std::runtime_error( "In invalid aztec decoding mode" );
    }
    for( int i = 0; i < nReceived; ++i )
    {
      message.push_back( characters[ i ] );
    }
  }
  return message;
}

#endif

