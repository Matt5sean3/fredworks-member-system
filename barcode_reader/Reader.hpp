
#include "Reader_fwd.h"
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <thread>
#include <atomic>

struct Reader_
{
public:
  Reader_( );
  Reader_( int device );
  virtual ~Reader_( );

  void start( );
  void stop( );

  void cycleCaptureSource( );
  void setFrameCallback( FrameCb cb, void * userdata );
  void setBarcodeCallback( BarcodeCb cb, void * userdata );

  std::vector< unsigned char > processBarcodeFrame( cv::Mat & frame );

  static void threadMethod( Reader * reader );

private:
  int captureIdentifier;
  cv::VideoCapture cap;
  std::thread readThread;
  std::atomic_bool halt;
  std::string device;

  FrameCb frameCb;
  void * frameCbUserdata;

  BarcodeCb barcodeCb;
  void * barcodeCbUserdata;
};



