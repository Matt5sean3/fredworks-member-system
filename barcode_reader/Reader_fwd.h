
#ifndef READER_FWD_H_
#define READER_FWD_H_

struct Reader_;
typedef struct Reader_ Reader;

typedef void ( * FrameCb )( const unsigned char * bitmap, int width, int height, void * userdata );
typedef void ( * BarcodeCb )( const unsigned char * message, int size, void * userdata );

#endif

