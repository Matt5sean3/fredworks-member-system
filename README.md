# FredWorks Member System

This repository contains the sources for software that can be used for a
makerspace membership system. This software is currently proposed for use at
the FredWorks Makerspace, but is still in active development.

# Compilation

## Dependencies

* OpenCV
* ZINT

# Installation

# Basic Usage

# Goals

The primary goal of this software is to provide a set of functionalities:

* Dues enforcement
* Member identification

There are also certain characteristics that are set as goals

* Minimize infrastructure
* Ease deployment
* Extensibility
* Cross-platform support

## Fulfillment of Goals

