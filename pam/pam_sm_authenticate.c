
#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_PASSWORD

#include <security/pam_modules.h>
#include <barcode_reader.h>

#define NUM_MESSAGES 2

struct barcode_login_info
{
};

// barcode callback
static void frame_callback( const unsigned char * message, int size, void * userdata )
{
  struct barcode_login_info * user_login_ptr;
  *( ( struct barcode_login_info * ) userdata )
  // user
  // password
}

// AUTH
PAM_EXTERN int
pam_sm_authenticate(
    pam_handle_t * pamh,
    int flags,
    int argc,
    char ** argv )
{
  // Don't have a database of authentication tokens for this authentication method
  int silent = flags & PAM_SILENT;
  int disallow_null_authtok = flags & PAM_DISALLOW_NULL_AUTHTOK;

  struct pam_message msg[ NUM_MESSAGES ];
  struct pam_response resp[ NUM_MESSAGES ];
  const struct pam_conv * conv;

  const void * ptr;
  int ret;
  if( ( ret = pam_get_item( pamh, PAM_CONV, &ptr ) ) != PAM_SUCCESS )
  {
    syslog( LOG_WARNING, "Missing conversation function: %s", pam_strerror( pamh, ret ) );
    return ret;
  }
  conv = ptr;

  msg[ 0 ].msg_style = PAM_TEXT_INFO;
  msg[ 0 ].msg = strdup( "Scan barcode to log in" );

  user = pam_get_item( pamh, PAM_USER, &ptr ) != PAM_SUCCESS ?
    NULL : ptr;

  if( user )
  {
    // Search for the barcode specific to that user
  }
  else
  {
    // no user was passed, base user off of whatever is scanned
  }
  free( msg[ 0 ].msg );

  // Start the barcode scanner
  Reader * barcode_reader = barcode_reader_create( 0 );

  int barcode_scanned = 0;

  while( ! barcode_scanned )
  {
    struct barcode_login_info user_login;

    barcode_reader_start( barcode_reader );
    barcode_reader_set_frame_callback( barcode_reader, frame_callback, &user_login );
    barcode_reader_stop( barcode_reader );

    // Send the message, wait for a response
    pamh->conv( 1, msg, resp, pamh->appdata_ptr );
  }
  barcode_reader_destroy( barcode_reader );

}

PAM_EXTERN int
pam_sm_setcred(
    pam_handle_t * pamh,
    int flags,
    int argc,
    char ** argv )
{
  return PAM_SUCCESS;
}

// ACCOUNT
PAM_EXTERN int
pam_sm_acct_mgmt(
    pam_handle_t * pamh,
    int flags,
    int argc,
    char ** argv )
{
  // Can check expiration on the account
  return PAM_SERVICE_ERR;
}

// PASSWORD
PAM_EXTERN int
pam_sm_chauthtok(
    pam_handle_t * pamh,
    int flags,
    int argc,
    char ** argv )
{
  // Doesn't have a means to change the auth token
  return PAM_SERVICE_ERR;
}

